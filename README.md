# LibreTag2

Libretag2 is an open source Laser Tag system based on ESP32 chips. 

![](Pictures/IMG_20190614_182304__01_resize_55.jpg)

## Features
* 240x320 full colour TFT touch screen
* Wireless mesh networking allowing all players to communicate with base controller
* Base beacons to re-spawn and reload ammunition
* Multiple player classes
* Realistic sound effects
* High power InfraRed used for shot transmission
* RGB LED hit sensors flash team colour
* Vibration motor in head sensor
* Multiple game modes (Team Play, CTF, Zombie, VIP, King Of The Hill)

## Its Easy
* A working gun + sensor combo should be able to be assembled by someone with basic electronic knowledge
* No tiny SMD components, stripboard should be possible
* Minimal use of external components and where ever possible should be standard easily obtainable parts
* Standard gun design is built mainly of common plumbing pipe and fittings, only small part is 3D printed

## Why ESP32
* The ESP32 chips have more than enough RAM and CPU power to do anything you could ever possibly want to implement in a laser tag system
* Cheap and readily available
* Some have built in lithium battery chargers
* Low sleep power consumption (Depends on Dev board)
* Wifi and Bluetooth
* Built in functionaility for audio, IR, LED's and PWM
* Sufficent IO pins

## Current Status
* Guns and head sensors working to the point that a full network mesh is obtained and players can shoot each other.
* Full health and ammo tracking working on guns with status updated on touch screen
* Proximity detection on base nodes to allow players to re-spawn

## History
The first LibreTag system was based on Picaxe chips and was developed in the 2009-2010 period. The picaxe chips whilst producing a succesfull laser tag system, have started to show their limitations.