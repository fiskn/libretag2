#include "ir.h"

uint32_t IR::encodeBullet(uint16_t playerID, uint16_t teamID, uint16_t gunType) {
  int type=1;
  return (type << 16) + (playerID << 8) + (teamID << 4) + gunType;
}

uint16_t IR::getPlayerFromBullet(uint32_t data) {
  return (data&0b001111111100000000)>>8;
}

uint16_t IR::getTeamFromBullet(uint32_t data) {
  return (data&0b000000000011110000)>>4;
}

uint16_t IR::getGunTypeFromBullet(uint32_t data) {
  return (data&0b000000000000001111);
}

/*
 * @brief Build register value of waveform for NEC one data bit
 */
inline void IR::fill_item_level(rmt_item32_t* item, int high_us, int low_us)
{
    item->level0 = 1;
    item->duration0 = (high_us) / 10 * RMT_TICK_10_US;
    item->level1 = 0;
    item->duration1 = (low_us) / 10 * RMT_TICK_10_US;
}

/*
 * @brief Generate NEC header value: active 9ms + negative 4.5ms
 */
void IR::fill_item_header(rmt_item32_t* item)
{
    fill_item_level(item, HEADER_HIGH_US, HEADER_LOW_US);
}

/*
 * @brief Generate NEC data bit 1: positive 0.56ms + negative 1.69ms
 */
void IR::fill_item_bit_one(rmt_item32_t* item)
{
    fill_item_level(item, BIT_ONE_HIGH_US, BIT_ONE_LOW_US);
}

/*
 * @brief Generate NEC data bit 0: positive 0.56ms + negative 0.56ms
 */
void IR::fill_item_bit_zero(rmt_item32_t* item)
{
    fill_item_level(item, BIT_ZERO_HIGH_US, BIT_ZERO_LOW_US);
}

/*
 * @brief Generate NEC end signal: positive 0.56ms
 */
void IR::fill_item_end(rmt_item32_t* item)
{
    fill_item_level(item, BIT_END, 0x7fff);
}

/*
 * @brief Check whether duration is around target_us
 */
inline bool IR::check_in_range(int duration_ticks, int target_us, int margin_us)
{
    //Serial.printf("%d,%d,%d\n",duration_ticks,target_us,margin_us);
    if(( ITEM_DURATION(duration_ticks) < (target_us + margin_us))
        && ( ITEM_DURATION(duration_ticks) > (target_us - margin_us))) {
        return true;
    } else {
        return false;
    }
}

/*
 * @brief Check whether this value represents an NEC header
 */
bool IR::header_if(rmt_item32_t* item)
{
    Serial.printf("%d,%d,%d,%d\n",item->level0,item->level1,item->duration0,item->duration1);
    if((item->level0 == RMT_RX_ACTIVE_LEVEL && item->level1 != RMT_RX_ACTIVE_LEVEL)
        && check_in_range(item->duration0, HEADER_HIGH_US, BIT_MARGIN)
        && check_in_range(item->duration1, HEADER_LOW_US, BIT_MARGIN)) {
        return true;
    }
    return false;
}

/*
 * @brief Check whether this value represents an NEC data bit 1
 */
bool IR::bit_one_if(rmt_item32_t* item)
{
    if((item->level0 == RMT_RX_ACTIVE_LEVEL && item->level1 != RMT_RX_ACTIVE_LEVEL)
        && check_in_range(item->duration0, BIT_ONE_HIGH_US, BIT_MARGIN)
        && check_in_range(item->duration1, BIT_ONE_LOW_US, BIT_MARGIN)) {
        return true;
    }
    
    return false;
}

/*
 * @brief Check whether this value represents an NEC data bit 0
 */
bool IR::bit_zero_if(rmt_item32_t* item)
{
    if((item->level0 == RMT_RX_ACTIVE_LEVEL && item->level1 != RMT_RX_ACTIVE_LEVEL)
        && check_in_range(item->duration0, BIT_ZERO_HIGH_US, BIT_MARGIN)
        && check_in_range(item->duration1, BIT_ZERO_LOW_US, BIT_MARGIN)) {
        return true;
    }
    return false;
}

uint8_t IR::calcChecksum(uint32_t data) {
  return(((data >> 12) + ((data >> 8) & 0xF) + ((data >> 4) & 0xF) +
         (data & 0xF)) & 0xF);
}

/*
 * @brief Parse NEC 32 bit waveform to address and command.
 */
int IR::parse_items(rmt_item32_t* item, int item_num, uint16_t* data)
{
    int w_len = item_num;
    if(w_len < DATA_ITEM_NUM) {
        Serial.printf("Invalid lenght: %d\n",w_len);
        return -1;
    }
    int i = 0, j = 0;
    if(!header_if(item)) {
        Serial.println("Invalid header");
        Serial.printf("%d,%d,%d,%d",item->level0,item->level1,item->duration0,item->duration1);
        return 0;

    }
    item++;
    uint16_t data_t = 0;
    for(j = 0; j < 22; j++) {
        if(bit_one_if(item)) {
            data_t |= (1 << j);
        } else if(bit_zero_if(item)) {
            data_t |= (0 << j);
        } else {
            //Serial.println("Invalid data");
            return -1;

        }
        item++;
        i++;
    }
    int checksum=data_t&0b1111;
    data_t=(data_t>>4);
    if(calcChecksum(data_t)!=checksum) {
      Serial.println("Invalid checksum");
      return -1;
    }
    *data = data_t;
    return i;
}

/*
 * @brief Build NEC 32bit waveform.
 */
int IR::build_items(int channel, rmt_item32_t* item, int item_num, uint16_t data)
{
    int i = 0, j = 0;
    if(item_num < DATA_ITEM_NUM) {
        return -1;
    }
    fill_item_header(item++);
    i++;
    for(j = 0; j < 22; j++) {
        if(data & 0x1) {
            fill_item_bit_one(item);
        } else {
            fill_item_bit_zero(item);
        }
        item++;
        i++;
        data >>= 1;
    }
    fill_item_end(item);
    i++;
    return i;
}

/*
 * @brief RMT transmitter initialization
 */
void IR::tx_init()
{
    rmt_config_t rmt_tx = RMT_DEFAULT_CONFIG_TX( RMT_TX_GPIO_NUM, RMT_TX_CHANNEL);
    rmt_tx.channel = RMT_TX_CHANNEL;
    rmt_tx.gpio_num = RMT_TX_GPIO_NUM;
    rmt_tx.mem_block_num = 1;
    rmt_tx.clk_div = RMT_CLK_DIV;
    rmt_tx.tx_config.loop_en = false;
    rmt_tx.tx_config.carrier_duty_percent = 33;
    rmt_tx.tx_config.carrier_freq_hz = 38000;
    rmt_tx.tx_config.carrier_level = RMT_CARRIER_LEVEL_HIGH;
    rmt_tx.tx_config.carrier_en = RMT_TX_CARRIER_EN;
    rmt_tx.tx_config.idle_level = RMT_IDLE_LEVEL_LOW;
    rmt_tx.tx_config.idle_output_en = true;
    rmt_tx.rmt_mode = RMT_MODE_TX;
    rmt_config(&rmt_tx);
    rmt_driver_install(rmt_tx.channel, 0, ESP_INTR_FLAG_SHARED|ESP_INTR_FLAG_LEVEL1|ESP_INTR_FLAG_IRAM);
}

/*
 * @brief RMT receiver initialization
 */
void IR::rx_init()
{
    rmt_config_t rmt_rx;
    rmt_rx.channel = RMT_RX_CHANNEL;
    rmt_rx.gpio_num = RMT_RX_GPIO_NUM;
    rmt_rx.clk_div = RMT_CLK_DIV;
    rmt_rx.mem_block_num = 2;
    rmt_rx.rmt_mode = RMT_MODE_RX;
    rmt_rx.rx_config.filter_en = true;
    rmt_rx.rx_config.filter_ticks_thresh = 100;
    rmt_rx.rx_config.idle_threshold = rmt_item32_tIMEOUT_US / 10 * (RMT_TICK_10_US);
    rmt_config(&rmt_rx);
    rmt_driver_install(rmt_rx.channel, 2000, 0);
    rmt_get_ringbuf_handle(rmt_rx.channel, &rb);
    rmt_rx_start(rmt_rx.channel, 1);
    rmt_set_gpio(rmt_rx.channel, rmt_rx.rmt_mode, rmt_rx.gpio_num, false);
}

int IR::rx_check()
{
esp_log_level_set(TAG, ESP_LOG_INFO);
  size_t rx_size = 0;
  rmt_item32_t *item = (rmt_item32_t*) xRingbufferReceive(rb, &rx_size, 0);
  if(item) {
      uint16_t rmt_data;
      if((rx_size / sizeof(rmt_item32_t))<20) //If packet is really short, don't even bother trying to parse it
        return 0;
      int res = parse_items(item, rx_size / sizeof(rmt_item32_t), &rmt_data);
      if(res > 0) {
          ESP_LOGD(TAG, "RMT RCV --- Data: 0x%04x", rmt_data);
          vRingbufferReturnItem(rb, item);
          return rmt_data;
      if(res==0) {
        vRingbufferReturnItem(rb, item);
          return 0;
      }
      } else {
          //play missed bullet sound
          vRingbufferReturnItem(rb, item);
          return -1;
      }
      //after parsing the data, return spaces to ringbuffer.
      vRingbufferReturnItem(rb, item);
    }
    return 0;
}


/**
 * @brief RMT transmitter demo, this task will periodically send NEC data. (100 * 32 bits each time.)
 *
 */
void IR::tx(uint16_t data)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);
    rmt_channel_t channel = RMT_TX_CHANNEL;
    ESP_LOGD(TAG, "RMT TX DATA");
    data=(data << 4) + calcChecksum(data);
    size_t size = (sizeof(rmt_item32_t) * DATA_ITEM_NUM);
    //each item represent a cycle of waveform.
    rmt_item32_t* item = (rmt_item32_t*) malloc(size);
    memset((void*) item, 0, size);
    int offset = 0;
    build_items(channel, item + offset, DATA_ITEM_NUM, data);
    //To send data according to the waveform items.
    rmt_write_items(channel, item, DATA_ITEM_NUM, true);
    //Wait until sending is done.
    //rmt_wait_tx_done(channel, portMAX_DELAY);
    //before we free the data, make sure sending is already done.
    free(item);
}
