#include <Arduino.h>
#include "esp_mesh.h"
#include "esp_mesh_internal.h"
#include "esp_event.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_pm.h"

extern int hw_type;

#define RX_SIZE          (1500)
#define TX_SIZE          (1460)
#define CONFIG_MESH_CHANNEL 6
#define CONFIG_MESH_ROUTER_SSID ""
#define CONFIG_MESH_ROUTER_PASSWD ""
#define CONFIG_MESH_AP_AUTHMODE WIFI_AUTH_OPEN
#define CONFIG_MESH_AP_CONNECTIONS 5
#define CONFIG_MESH_AP_PASSWD "L1breTag"
#define CONFIG_MESH_MAX_LAYER 10
#define MESH_SET_NODE
/*MeshPacket
0 = NULL
1 = ID Request
2 = ID Offer
3 = AD Advertise
4 = Sensor Recieve IR
5 = Sensor Command (Tell Sensor to do something)
6 = Sensor Registraion with Gun
7 = Battery Data (Gun & Sensor)
8 = Player Updates (GPS...etc)
9 = Score Update
10 = Game Type/info broadcast from root
11 = Beacons (Respawn/Ammo Dump)
12= Gun Commands
13 = Sensor Updates
*/
enum MeshPacket {PKT_NULL, PKT_IDReq, PKT_IDOffer, PKT_IDAdv, PKT_SensorRX, PKT_SensorCmd, PKT_SensorReg,PKT_Battery,PKT_PlayerUpdate, PKT_ScoreUpdate, PKT_GameType, PKT_Beacon, PKT_GunCMD, PKT_SensorUpd};
enum MeshState {MESH_NULL, MESH_DISCONNECTED, MESH_CONNECT_NO_ID, MESH_CONNECTED, MESH_CONNECTED_OOR};
enum GameTypes {GT_NULL, GT_FreeForAll, GT_Teams, GT_KOTH};
enum BeaconCode {BC_NULL, BC_RESPAWN};

static const uint8_t MESH_ID[6] = { 0x77, 0x77, 0x77, 0x77, 0x77, 0x77};
static const uint8_t ROUTER_BSSID[6] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
extern mesh_addr_t Broadcast_addr;
extern mesh_addr_t mesh_parent_addr;
extern uint8_t tx_buf[TX_SIZE];
extern uint8_t rx_buf[RX_SIZE];
extern int mesh_layer;
extern mesh_addr_t SensorAddr;
extern int MeshState;
extern uint8_t beaconCode;
extern bool wifiRescan,beaconDiscoveryOnly;
extern wifi_scan_config_t scan_config;
void InitMesh();
void meshTX(mesh_addr_t to);
void mesh_scan_done_handler(int num);
void mesh_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
void ip_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
