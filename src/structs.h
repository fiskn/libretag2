#include <Arduino.h>
#include <esp_mesh.h>
#ifndef MYSTRUCT_H
 #define MYSTRUCT_H
struct PlayerDetail {
  double lat;
  double lon;
  uint16_t shots;
  uint32_t gpsAge;
  mesh_addr_t mac;
  char macAddr[19];
  uint8_t hitcount[32];
  uint8_t deaths;
  uint8_t macassigned=0;
  uint32_t lastseen;
  uint8_t battery;
  uint8_t team;
};
extern PlayerDetail PlayerDetails[32];

struct node { 
    char data[40] ; 
    struct node* next; 
}; 
typedef struct node Node;
typedef struct node* NodePointer;
 #endif