#include <stdio.h>
#include <Arduino.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "esp_log.h"
#include "driver/rmt.h"
#include "driver/periph_ctrl.h"
#include "soc/rmt_reg.h"


//CHOOSE SELF TEST OR NORMAL TEST
#define RMT_RX_SELF_TEST   0

#if RMT_RX_SELF_TEST
#define RMT_RX_ACTIVE_LEVEL  1   /*!< Data bit is active high for self test mode */
#define RMT_TX_CARRIER_EN    0   /*!< Disable carrier for self test mode  */
#else
//Test with infrared LED, we have to enable carrier for transmitter
//When testing via IR led, the receiver waveform is usually active-low.
#define RMT_RX_ACTIVE_LEVEL  0   /*!< If we connect with a IR receiver, the data is active low */
#define RMT_TX_CARRIER_EN    1   /*!< Enable carrier for IR transmitter test with IR led */
#endif

#define RMT_TX_CHANNEL   RMT_CHANNEL_7     /*!< RMT channel for transmitter */
#define RMT_TX_GPIO_NUM  GPIO_NUM_33     /*!< GPIO number for transmitter signal */
#define RMT_RX_CHANNEL   RMT_CHANNEL_6     /*!< RMT channel for receiver */
#define RMT_RX_GPIO_NUM  GPIO_NUM_27     /*!< GPIO number for receiver */
#define RMT_CLK_DIV      80    /*!< RMT counter clock divider */
#define RMT_TICK_10_US    (80000000/RMT_CLK_DIV/100000)   /*!< RMT counter value for 10 us.(Source clock is APB clock) */

#define HEADER_HIGH_US    2400                         /*!< NEC protocol header: positive 2.4ms */
#define HEADER_LOW_US     1120                         /*!< NEC protocol header: negative 4.5ms*/
#define BIT_ONE_HIGH_US    560                         /*!< NEC protocol data bit 1: positive 0.56ms */
#define BIT_ONE_LOW_US    (2250-BIT_ONE_HIGH_US)   /*!< NEC protocol data bit 1: negative 1.69ms */
#define BIT_ZERO_HIGH_US   560                         /*!< NEC protocol data bit 0: positive 0.56ms */
#define BIT_ZERO_LOW_US   (1120-BIT_ZERO_HIGH_US)  /*!< NEC protocol data bit 0: negative 0.56ms */
#define BIT_END            560                         /*!< NEC protocol end: positive 0.56ms */
#define BIT_MARGIN         200                          /*!< NEC parse margin time */

#define ITEM_DURATION(d)  ((d & 0x7fff)*10/RMT_TICK_10_US)  /*!< Parse duration time from memory register value */
#define DATA_ITEM_NUM   24  /*!< NEC code item number: header + 32bit data + end */
#define RMT_TX_DATA_NUM  100    /*!< NEC tx test data number */
#define rmt_item32_tIMEOUT_US  9500   /*!< RMT receiver timeout value(us) */

class IR {
private:
  RingbufHandle_t rb = NULL;
  const char* TAG = "IR";
  static uint8_t calcChecksum(uint32_t data);
  static inline void fill_item_level(rmt_item32_t* item, int high_us, int low_us);
  static void fill_item_header(rmt_item32_t* item);
  static void fill_item_bit_one(rmt_item32_t* item);
  static void fill_item_bit_zero(rmt_item32_t* item);
  static void fill_item_end(rmt_item32_t* item);
  static inline bool check_in_range(int duration_ticks, int target_us, int margin_us);
  static bool header_if(rmt_item32_t* item);
  static bool bit_one_if(rmt_item32_t* item);
  static bool bit_zero_if(rmt_item32_t* item);
  static int parse_items(rmt_item32_t* item, int item_num, uint16_t* data);
  static int build_items(int channel, rmt_item32_t* item, int item_num, uint16_t data);

public:
  static void tx_init();
  void rx_init();
  int rx_check();
  void tx(uint16_t data);
  uint32_t encodeBullet(uint16_t playerID, uint16_t teamID, uint16_t gunType);
  uint16_t getPlayerFromBullet(uint32_t data);
  uint16_t getTeamFromBullet(uint32_t data);
  uint16_t getGunTypeFromBullet(uint32_t data);
  static void rmt_example_rx_task();
  static void rmt_example_tx_task();

};
