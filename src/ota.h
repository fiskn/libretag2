#include <Arduino.h>
#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#include <TFT_Drivers/ST7735_Defines.h>
#include <stdint.h>
#include <algorithm>
#include <SPI.h>
#include <SPIFFS.h>
#include <HTTPClient.h>
#include <Update.h>
#include <WiFi.h>

extern TFT_eSPI tft;

class ota {
private:
  int retries = 10;
  void downloadFile(char* url, char* filename);
public:
  int connectWifi();
  int flashFirmware();
  int downloadFiles();
};
