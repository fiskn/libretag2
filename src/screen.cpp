#include "screen.h"
#include "console.h"
#include "mesh.h"
#include <EspNowFloodingMesh.h>
#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h>

#define CALIBRATION_FILE "/TouchCalData"
#define REPEAT_CAL false
#define TAG "Screen"

#ifndef LCD_COLS
#define LCD_COLS 20
#endif

#ifndef LCD_ROWS
#define LCD_ROWS 4
#endif

#define LCD 1
hd44780_I2Cexp lcd;


void InitScreen(){
  ESP_LOGI(TAG, "Init Screen");
  if(LCD){
    digitalWrite(4,1);
    ledcAttachPin(13, 7);
    ledcSetup(7, 20000, 8);
    ledcWrite(7, 160);
    ledcAttachPin(5, 6);
    ledcSetup(6, 20000, 8);
    ledcWrite(6, 160);
    Wire.begin(18, 19);
    lcd.begin(LCD_COLS, LCD_ROWS);
    //lcd.print("Hello, World!");
    ESP_LOGI(TAG, "LCD Done");
  } else {
    
    tft.init();
    tft.setRotation(2);
    tft.fillScreen(TFT_BLACK);
    ESP_LOGI(TAG, "TFT Done");
  }
}

void updateScreen(void * parameter) {
  uint32_t screenUpdate;
  char mystring[40];
    while(1)
    {
        xSemaphoreGive(xTFTMutex);
        xTaskNotifyWait( ULONG_MAX,      /* Clear any notification bits on entry. */
                         ULONG_MAX, /* Reset the notification value to 0 on exit. */
                         &screenUpdate, /* Notified value pass out in
                                              ulNotifiedValue. */
                         portMAX_DELAY );  /* Block indefinitely. */

        xSemaphoreTake(xTFTMutex, portMAX_DELAY);
        ESP_LOGD(TAG, "Updating Screen With: %d",screenUpdate);
        if((screenUpdate & SU_Wifi) == SU_Wifi)
        {
          if(MeshState==MESH_CONNECTED) {
            if(!LCD){
              //sprintf(mystring,"rssi:%d\r\n", wifidata.rssi);
              //tft.fillRect(0, 100, 100, 150, TFT_BLACK);
              tft.setTextSize(1);
              tft.setFreeFont(&FreeSans12pt7b);
              tft.setTextColor(TFT_GREEN, TFT_BLACK);
              //tft.drawString(mystring, 5, 125, 1);
              tft.fillRect(220, 10, 3, 5, TFT_RED);
              tft.fillRect(225, 7, 3, 8, TFT_RED);
              tft.fillRect(230, 4, 3, 11, TFT_RED);
              tft.fillRect(235, 1, 3, 14, TFT_RED);
              if(lastrssi>160)
                tft.fillRect(220, 10, 3, 5, TFT_GREEN);
              if(lastrssi>170)
                tft.fillRect(225, 7, 3, 8, TFT_GREEN);
              if(lastrssi>180)
                tft.fillRect(230, 4, 3, 11, TFT_GREEN);
              if(lastrssi>190)
                tft.fillRect(235, 1, 3, 14, TFT_GREEN);
            }
          }
          else {
            if(!LCD){
              tft.fillRect(220, 10, 3, 5, TFT_DARKGREY);
              tft.fillRect(225, 7, 3, 8, TFT_DARKGREY);
              tft.fillRect(230, 4, 3, 11, TFT_DARKGREY);
              tft.fillRect(235, 1, 3, 14, TFT_DARKGREY);
            }
          }
          if(SensorConnected)
          {
            if(!LCD){
              tft.fillCircle(180,10,5,TFT_GREEN);
            }
          }
          else
          {
            if(!LCD){
              tft.fillCircle(180,10,5,TFT_RED);
            }
          }
          if(gps_connected)
          {
            if(!LCD){
              tft.fillCircle(170,10,5,TFT_GREEN);
            }
          }
          else
          {
            if(!LCD){
              tft.fillCircle(170,10,5,TFT_RED);
            }
          }
        }
        if((screenUpdate & SU_Bullets) == SU_Bullets)
        {
          if(!LCD){
            tft.setTextDatum(TL_DATUM);
            tft.setTextSize(1);
            tft.setFreeFont(&FreeSans18pt7b);
            tft.setTextColor(TFT_GREEN, TFT_BLACK);
            sprintf(mystring, "%2d", bullets);
            tft.drawString(mystring, 20, 265, 1);
            tft.setFreeFont(&FreeSans12pt7b);
            sprintf(mystring, "%3d", bulletTotal);
            tft.drawString(mystring, 20, 295, 1);
          }else{ //LCD Screen
            lcd.setCursor(0,2);
            lcd.printf("Rnds %2d |/%3d",bullets,bulletTotal);
          }
        }
        if((screenUpdate & SU_Health) == SU_Health)
        {
          if(!LCD){
            tft.setTextDatum(TL_DATUM);
            tft.setTextSize(1);
            tft.setFreeFont(&FreeSans18pt7b);
            tft.setTextColor(TFT_GREEN, TFT_BLACK);
            sprintf(mystring, "%3d", health);
            tft.drawString(mystring, 180, 290, 1);
          }else{ //LCD Screen
            lcd.setCursor(0,1);
            lcd.printf("Hlth %3d|",health);
          }
        }
        if((screenUpdate & SU_Reload) == SU_Reload)
        {
          if(!LCD){
            tft.setTextDatum(TL_DATUM);
            tft.drawRect(60, 260, 120, 50, TFT_WHITE);
            tft.fillRect(61, 261, 118, 48, TFT_BLACK);
            tft.fillRect(61, 261, (reloadCounter*10)-2, 48, TFT_RED);
            tft.setTextSize(1);
            tft.setFreeFont(&FreeSans18pt7b);
            tft.setTextColor(TFT_WHITE);
            tft.drawString("Reload", 65, 270, 1);
          }else{ //LCD Screen
            lcd.setCursor(0,3);
            lcd.printf("Reload  |");
          }
        }
        if((screenUpdate & SU_Battery) == SU_Battery)
        {
          if(!LCD){
            int SensorBattery_pct;
            SensorBattery_pct=(1000-(4200-SensorBattery))/10;
            tft.fillRect(200,0,6,5,TFT_WHITE);
            tft.drawRect(197,4,12,20,TFT_WHITE);
            tft.fillRect(198, 5, 10, 18, TFT_BLACK);
            tft.fillRect(198, 23-(battery_pct/5), 10, (battery_pct/5), TFT_GREEN);
            tft.fillRect(100,0,6,5,TFT_WHITE);
            tft.drawRect(97,4,12,20,TFT_WHITE);
            tft.fillRect(98, 5, 10, 18, TFT_BLACK);
            tft.fillRect(98, 23-(SensorBattery_pct/5), 10, (SensorBattery_pct/5), TFT_GREEN);
            tft.setTextSize(1);
            tft.setFreeFont(&FreeSans12pt7b);
            tft.setTextColor(TFT_GREEN, TFT_BLACK);
            sprintf(mystring, "%dv,%d", voltage,battery_pct);
            tft.drawString(mystring, 5, 180, 1);
            telemetry_stats_st *telemetry_stats;
            telemetry_stats=espNowFloodingMesh_get_tmt_stats_ptr();
            sprintf(mystring, "%d,%d,%d,%d,%d",telemetry_stats->dup_pkt,telemetry_stats->fwd_pkt,telemetry_stats->received_pkt,telemetry_stats->sent_pkt,telemetry_stats->err_pkt );
            tft.drawString(mystring, 5, 200, 1);
          }
        }
        if((screenUpdate & SU_Taskbar) == SU_Taskbar)
        {
          if(!LCD){
            //drawJpeg("/bullet.jpg", 0, 260);
            TJpgDec.drawFsJpg(0,260,"/bullet.jpg");
            //drawJpeg("/heart.jpg", 200, 260);
            TJpgDec.drawFsJpg(200,260,"/heart.jpg");
            //drawJpeg("/player.jpg", 0, 0);
            TJpgDec.drawFsJpg(0,0,"/player.jpg");
            //drawJpeg("/team.jpg", 50, 0);
            TJpgDec.drawFsJpg(50,0,"/team.jpg");
            //drawJpeg("/info.jpg",0,25);
            TJpgDec.drawFsJpg(0,25,"/info.jpg");
            //drawJpeg("/settings.jpg",48,25);
            TJpgDec.drawFsJpg(48,25,"/settings.jpg");
            //drawJpeg("/map.jpg",96,25);
            TJpgDec.drawFsJpg(95,25,"/map.jpg");
            //drawJpeg("/power.jpg",192,25);
            TJpgDec.drawFsJpg(192,25,"/power.jpg");
            tft.setTextDatum(TL_DATUM);
            tft.setTextSize(1);
            tft.setFreeFont(&FreeSans12pt7b);
            tft.setTextColor(TFT_GREEN, TFT_BLACK);
            tft.fillRect(20,1,20,20,TFT_BLACK);
            tft.fillRect(80,1,20,20,TFT_BLACK);
            sprintf(mystring, "%2d", playerID);
            tft.drawString(mystring, 20, 1, 1);
            sprintf(mystring, "%d", teamID);
            tft.drawString(mystring, 80, 1, 1);
            //Line below taskbar
            tft.drawLine(0,25,240,25,TFT_WHITE);
            //Line above reload button
            tft.drawLine(0,260,240,260,TFT_WHITE);
            //Line above game status
            tft.drawLine(0,235,240,235,TFT_WHITE);
          }else{ //LCD Screen
            lcd.setCursor(0,0);
            lcd.printf("Plyr %2d |Team%d",playerID,teamID);
          }
        }
        if((screenUpdate & SU_Main) == SU_Main)
        {
          if(!LCD){
            if(ScreenMode==1) { //Info
              int j=0;
              NodePointer current=head;
              tft.drawCircle(24,49,24,TFT_YELLOW);
              tft.drawCircle(24,49,23,TFT_YELLOW);
              tft.fillRect(0,75,240,160,TFT_BLACK);
              tft.setFreeFont(&FreeSans9pt7b);
              tft.setTextSize(1);
              while(NULL != current){
                sprintf(mystring, "%s",current->data);
                tft.drawString(mystring, 0, 75+(j*14), 2);
                current = current->next;
                j++;
              }
            }
            if(ScreenMode==2) { //Settings
              tft.drawCircle(72,49,24,TFT_YELLOW);
              tft.drawCircle(72,49,23,TFT_YELLOW);
              tft.fillRect(5,85,115,30,TFT_WHITE);
              tft.setTextSize(1);
              tft.setFreeFont(&FreeSans12pt7b);
              tft.setTextColor(TFT_BLACK, TFT_WHITE);
              sprintf(mystring, "Fire Mode");
              tft.drawString(mystring, 8, 90, 1);
              tft.setTextColor(TFT_WHITE, TFT_BLACK);
              if(FireMode==0)
                sprintf(mystring, "(Burst)");
              if(FireMode==1)
                sprintf(mystring, "(Auto)");
              if(FireMode==2)
                sprintf(mystring, "(Single)");
              tft.drawString(mystring, 130, 90, 1);
              if(!Supressor) {
                tft.fillRect(5,125,115,30,TFT_RED);
                tft.setTextColor(TFT_BLACK, TFT_RED);
              }
              else {
                tft.fillRect(5,125,115,30,TFT_GREEN);
                tft.setTextColor(TFT_BLACK, TFT_GREEN);
              }
              sprintf(mystring, "Silencer");
              tft.drawString(mystring, 8, 130, 1);
              if(!Laser) {
                tft.fillRect(5,165,115,30,TFT_RED);
                tft.setTextColor(TFT_BLACK, TFT_RED);
              }
              else {
                tft.fillRect(5,165,115,30,TFT_GREEN);
                tft.setTextColor(TFT_BLACK, TFT_GREEN);
              }
              sprintf(mystring, "Laser");
              tft.drawString(mystring, 8, 140, 1);
            }
            if(ScreenMode==3) { //Map
              tft.drawCircle(120,49,24,TFT_YELLOW);
              tft.drawCircle(120,49,23,TFT_YELLOW);
              tft.fillRect(0,75,240,180,TFT_BLACK);
              tft.fillCircle(120,180,10,TFT_YELLOW);
              for(int i=0;i<32;i++) {
                if(PlayerDetails[i].gpsAge<10000&&i!=playerID&&PlayerDetails[i].lat!=0) {
                  int x,y;
                  double distance,angle;
                  distance=gps.distanceBetween(lat,lon,PlayerDetails[i].lat,PlayerDetails[i].lon);
                  angle=gps.courseTo(lat,lon,PlayerDetails[i].lat,PlayerDetails[i].lon);
                  x=(int)(120+(sin(angle)*distance));
                  y=(int)(180-(cos(angle)*distance));
                  ESP_LOGI(TAG, "MyLat:%f MyLon:%f HisLat:%f HisLon:%f Distance:%f Angle %f",lat,lon,PlayerDetails[i].lat,PlayerDetails[i].lon,distance,angle);
                  if(x>0&&x<240&&y>50&&y<250){
                    tft.fillCircle(x,y,10,TFT_RED);
                  }
                }
              }
            }
          }
        }
        if((screenUpdate & SU_Dead) == SU_Dead)
        {
          if(!LCD){
            sprintf(mystring, "%d",deathCounter);
            tft.setTextSize(4);
            tft.setFreeFont(&FreeSans24pt7b);
            tft.setTextColor(TFT_BLACK, TFT_WHITE);
            tft.drawString(mystring, 30, 78, 1);
          }
        }
        if((screenUpdate & SU_Score) == SU_Score)
        {
          if(!LCD){
            tft.setTextSize(1);
            tft.setFreeFont(&FreeSans12pt7b);
            sprintf(mystring, "%d",ScoreTeam1);
            tft.setTextColor(TFT_RED, TFT_BLACK);
            tft.drawString(mystring, 50, 237, 1);
            sprintf(mystring, "<-Score->");
            tft.setTextColor(TFT_WHITE, TFT_BLACK);
            tft.drawString(mystring, 70, 237, 1);
            sprintf(mystring, "%d",ScoreTeam2);
            tft.setTextColor(TFT_GREEN, TFT_BLACK);
            tft.drawString(mystring, 180, 237, 1);
          }
        }
        if((screenUpdate & SU_Temp) == SU_Temp)
        {
          if(!LCD){
            tft.fillRect(230,100,9,100,TFT_BLACK);
            tft.fillRect(230,200-((barreltemp/10)),9,(barreltemp/10),TFT_RED);
            tft.drawRect(230,100,10,100,TFT_WHITE);
          }
        }
    }
}

void touch_calibrate()
{
  if(LCD)
    return;
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check if calibration file exists and size is correct
  if (SPIFFS.exists(CALIBRATION_FILE)) {
    ESP_LOGI(TAG, "Touch Calibration Data File Exists");
    if (REPEAT_CAL||runtime_cal)
    {
      // Delete if we want to re-calibrate
      ESP_LOGI(TAG, "Recalibrating - Deleting data");
      SPIFFS.remove(CALIBRATION_FILE);
    }
    else
    {
      ESP_LOGI(TAG, "Loading calibration data");
      File f = SPIFFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }

  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = SPIFFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}

bool tft_output(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t* bitmap)
{
   // Stop further decoding as image is running off bottom of screen
  if ( y >= tft.height() ) return 0;

  // This function will clip the image block rendering automatically at the TFT boundaries
  tft.pushImage(x, y, w, h, bitmap);

  // This might work instead if you adapt the sketch to use the Adafruit_GFX library
  // tft.drawRGBBitmap(x, y, bitmap, w, h);

  // Return 1 to decode next block
  return 1;
}