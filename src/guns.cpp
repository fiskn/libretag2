#include "sensors.h"
#include <Arduino.h>
#include <ArduinoJson.h>
#include <EspNowFloodingMesh.h>
#include "mesh.h"
#include <FastLED.h>
#include "ir.h"
#include "AudioFileSourceSPIFFS.h"
#include "AudioGeneratorWAV.h"
#include "AudioOutputI2SNoDAC.h"
#include "AudioOutputI2S.h"
#include "AudioOutputMixer.h"
#include "AudioGeneratorTalkie.h"
#include <TinyGPS++.h>
#include "console.h"
#include "screen.h"

#define TAG "Gun"
#define MESH_TAG "Mesh"
#define NUM_GUN_LEDS 9

extern int playerID,teamID,health,bullets,bulletTotal,battery_pct,SensorConnected,runtime_cal,counter,barreltemp;
extern long bulletdelay,everySecond,deathDelay,reloadDelay,gpsDelay,gunsDelay,meshDelay,rootDelay,wifiScanDelay;
extern uint32_t LastAction, lastSensorPKT, lastMeshPKT;
extern int SensorBattery;
extern uint8_t ScreenMode, FireMode, ButtonRelease,TouchRelease,Supressor, GameType, beaconCode, Laser, lastrssi;
extern CRGB gun_leds[NUM_GUN_LEDS];
extern class IR IR;
extern AudioGeneratorTalkie *talkie;

extern char myMacString[18];
extern char broadcastString[18];
char sensorMac[18];
int shots;

void playWav(const char *wavfilename);
void PowerOff();

void shoot() {
  shots++;
  int IRdata=0;
  if(Laser) {
    playWav("/laser.wav");
    FastLED.setBrightness(255);
    for(int i=0;i<8;i++) {
      gun_leds[i] = CRGB::Green;
      //head_leds[0] = CRGB::Yellow;
    }
    FastLED.show();
  }
  else {
    if(!Supressor) {
      playWav("/shot.wav");
      FastLED.setBrightness(255);
      digitalWrite(22,1);
      for(int i=0;i<8;i++) {
        gun_leds[i] = CRGB::White;
        //head_leds[0] = CRGB::Yellow;
      }
      FastLED.show();
    }
    else {
      playWav("/supress.wav");
    }
  }
  bullets-=1;
  bulletdelay=millis();
  if(barreltemp<1000) {
    if(!Supressor)
      barreltemp+=20;
    else
      barreltemp+=40;
  }
  
  IRdata=IR.encodeBullet(playerID, teamID, 1);
  //delay(10); //This delay is needed otherwise shooting seems to hang for around 5s for no apparent reason????
  //ESP_LOGI(MESH_TAG, "Before IR");
  IR.tx(IRdata);
  //ESP_LOGI(MESH_TAG, "After IR");
  //delay(10); //This delay is needed otherwise shooting seems to hang for around 5s for no apparent reason????
  xTaskNotify( xUpdateScreen, SU_Bullets, eSetBits );
  //ESP_LOGI(MESH_TAG, "After Screen");
  if(!Laser) {
    if(barreltemp>300)
      FastLED.setBrightness((barreltemp-300)/4);
    else
      FastLED.setBrightness(0);
    for(int i=0;i<8;i++)
      gun_leds[i] = CRGB::Red;
    //delay(20);
    FastLED.show();
  }
  digitalWrite(22,0);
  ESP_LOGI(TAG, "Mem: %d",esp_get_free_heap_size());
  //ESP_LOGI(MESH_TAG, "Shooting Done");
}

void guns_net_rx(const uint8_t *data, int len, uint32_t replyPrt, uint8_t rssi) {
  uint16_t x, y;
  int flag = 0;
  ESP_LOGI(MESH_TAG, "Mesh data recieved:%s - RSSI:%d", data,rssi);
  StaticJsonDocument<200> pkt;
  deserializeJson(pkt, data);
  char* to;
  if(pkt.containsKey("to"))
    to=pkt["to"];
  else
    to = "00:00:00:00:00:00";
  //ESP_LOGI(MESH_TAG, "me:%c to:%c bc:%c",myMacString[0],to[0],broadcastString[0]);
  //ESP_LOGI(MESH_TAG, "Compare1:%d Compare2:%d",memcmp(&myMacString,to,1),memcmp(&broadcastString,to,1));
  if(memcmp(&myMacString,to,sizeof(myMacString))!=0&&memcmp(&broadcastString,to,sizeof(broadcastString))!=0){
    //ESP_LOGI(MESH_TAG, "Pkt not for me!!");
    return;
  }
  char* from;
  if(pkt.containsKey("from"))
    from=pkt["from"];
  else
    from = "00:00:00:00:00:00";
  
  if(memcmp(&sensorMac,from,sizeof(sensorMac))==0){
    lastSensorPKT=millis();
  }
  if(memcmp(&sensorMac,from,sizeof(sensorMac))!=0) {
    lastMeshPKT=millis();
    lastrssi=rssi;
  }
  switch(pkt["type"].as<unsigned char>()){
    case PKT_IDOffer: {
      playerID=pkt["PlayerID"];
      teamID=pkt["TeamID"];
      ESP_LOGI(MESH_TAG, "PlayerID set to: %d", playerID);
      consoleInsert(&head,"Joined Game as Player:%d in Team:%d",playerID,teamID);
      MeshState=MESH_CONNECTED;
      xTaskNotify( xUpdateScreen, SU_Taskbar, eSetBits );
    }
    break;
    case PKT_SensorReg: {
      if(rssi>200&&playerID!=0&&teamID!=0){
        ESP_LOGI(MESH_TAG, "Sensor Connected");
        playWav("/snsrcnt.mp3");
        SensorConnected=1;
        //strcpy(sensorMac,pkt["from"]);
        memcpy(&sensorMac,from,sizeof(sensorMac));
        xTaskNotify( xUpdateScreen, SU_Wifi, eSetBits );
        StaticJsonDocument<200> pkt;
        pkt["type"]=PKT_SensorCmd;
        pkt["SensorCmd"]=1;
        pkt["PlayerID"]=playerID;
        pkt["TeamID"]=teamID;
        pkt["from"]=myMacString;
        pkt["to"]=from;
        char message[128];
        serializeJson(pkt, message);
        espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
      }
    }
    break;
    case PKT_SensorRX: {
      if(health==0)
        break;
      
      
      if(pkt["hit"].as<unsigned char>()==0) {
        playWav("/miss.wav");
        break;
      }
      int hitByPlayer=pkt["player"].as<int>(),hitByTeam=pkt["team"].as<int>() ;
      StaticJsonDocument<200> pkt;
      pkt["type"]=PKT_SensorCmd;
      if((health-10)>0) {
          health-=10;
          PlayerDetails[playerID].hitcount[hitByPlayer]++;
          xTaskNotify( xUpdateScreen, SU_Health, eSetBits );
          playWav("/damage.wav");
          pkt["SensorCmd"]=2;
          if(teamID==1) {
            pkt["Red"]=255;
            pkt["Green"]=0;
            pkt["Blue"]=0;
          }
          else {
            pkt["Red"]=0;
            pkt["Green"]=255;
            pkt["Blue"]=0;
          }
        }
      else {
        health=0;
        PlayerDetails[playerID].deaths++;
        //deathCounter=20;
        pkt["SensorCmd"]=3;
        xTaskNotify( xUpdateScreen, SU_Health, eSetBits );
        srand (time(NULL));
        int randomPhrase=rand()%5+1;
        switch (randomPhrase) {
          case 1: {
            consoleInsert(&head,"Player:%d smoked player:%d",hitByPlayer,playerID);
          }
          break;
          case 2: {
            consoleInsert(&head,"Player:%d felt player:%d's pain",playerID,hitByPlayer);
          }
          break;
          case 3: {
            consoleInsert(&head,"Player:%d killed player:%d",hitByPlayer,playerID);
          }
          break;
          case 4: {
            consoleInsert(&head,"Player:%d sent player:%d away",hitByPlayer,playerID);
          }
          break;
          case 5: {
            consoleInsert(&head,"Player:%d owned player:%d",hitByPlayer,playerID);
          }
          break;
        }
        
        playWav("/die.wav");
        ESP_LOGI(TAG,"Die");
        //die
      }
      pkt["from"]=myMacString;
      pkt["to"]=from;
      char message[128];
      serializeJson(pkt, message);
      espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
    }
    break;
    case PKT_SensorUpd: {
      if(SensorConnected){
        SensorBattery=pkt["Battery"].as<int>();
      }
      else
      {
        if(MeshState==MESH_CONNECTED){
          ESP_LOGI(MESH_TAG, "Sensor Connected");
          lastSensorPKT=millis();
          playWav("/snsrcnt.wav");
          SensorConnected=1;
          strcpy(sensorMac,pkt["from"]);
          xTaskNotify( xUpdateScreen, SU_Wifi, eSetBits );
          /*StaticJsonDocument<200> pkt;
          pkt["type"]=PKT_SensorCmd;
          pkt["SensorCmd"]=1;
          pkt["PlayerID"]=playerID;
          pkt["TeamID"]=teamID;
          pkt["from"]=myMacString;
          pkt["to"]=from;
          char message[128];
          serializeJson(pkt, message);
          espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);*/
        }
      }
    }
    break;
    case PKT_PlayerUpdate: {
      int id=pkt["PlayerID"].as<int>();
      PlayerDetails[id].lat=pkt["GPSLat"].as<double>();
      PlayerDetails[id].lon=pkt["GPSLon"].as<double>();
      PlayerDetails[id].gpsAge=pkt["GPSAge"].as<int>();
    }
    break;
    case PKT_GameType: {
      GameType=pkt["GameType"].as<int>();
      lastrssi=rssi;
    }
    break;
    case PKT_ScoreUpdate: {
      ScoreTeam1=pkt["Team1"].as<int>();
      ScoreTeam2=pkt["Team2"].as<int>();
      xTaskNotify( xUpdateScreen, SU_Score, eSetBits );
    }
    break;
    case PKT_Beacon: {
      if(pkt["BeaconCmd"].as<int>()==BC_RESPAWN&&pkt["TeamID"].as<int>()==teamID&&rssi>195){
        playWav("/beep.wav");
        xTaskNotify( xUpdateScreen, SU_Bullets, eSetBits );
        if(health==0) {
          StaticJsonDocument<200> pkt;
          pkt["type"]=PKT_SensorCmd;
          pkt["SensorCmd"]=4;
          pkt["from"]=myMacString;
          pkt["to"]=sensorMac;
          char message[128];
          serializeJson(pkt, message);
          espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
          health=100;
          bullets=30;
          bulletTotal=300;
          playWav("/respawn.wav");
          xTaskNotify( xUpdateScreen, SU_Health|SU_Bullets, eSetBits );
        }
        else {
          if(bulletTotal<290)
            bulletTotal=+10;
          else
            bulletTotal=300;
        }
      }
      if(pkt["BeaconCmd"].as<int>()==BC_RESPAWN&&pkt["TeamID"].as<int>()!=teamID) {
      //Enemy base, capture flag or something to be implemented here
      }
    }
    break;
    case PKT_GunCMD: {
      if(pkt["cmd"].as<int>()==1)
        ESP.restart();
    }
    break;
    default:
      //ESP_LOGE(MESH_TAG, "Unhandled Packet");
    break;
  }
  vTaskDelay(10);
}


void guns_loop() {
  uint16_t x, y;
  int flag = 0;
  if(MeshState==MESH_CONNECT_NO_ID&&millis()>(wifiScanDelay+1000)) {
    wifiScanDelay=millis();
    StaticJsonDocument<200> pkt;
    pkt["type"]=PKT_IDReq;
    pkt["from"]=myMacString;
    pkt["to"]="FF:FF:FF:FF:FF:FF";
    char message[128];
    serializeJson(pkt, message);
    espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 3);
    ESP_LOGI(MESH_TAG, "Requesting ID");
  }
  if(lastSensorPKT+10000<millis()) {
    SensorConnected=0;
    xTaskNotify( xUpdateScreen, SU_Wifi, eSetBits );
  }
  if(MeshState!=MESH_CONNECT_NO_ID){
    if(lastMeshPKT+10000<millis()) {
      MeshState=MESH_CONNECTED_OOR;
      xTaskNotify( xUpdateScreen, SU_Wifi, eSetBits );
    }
    else if(MeshState==MESH_CONNECTED_OOR){
      MeshState=MESH_CONNECTED;
      xTaskNotify( xUpdateScreen, SU_Wifi, eSetBits );
    }
  }
  xSemaphoreTake(xTFTMutex, portMAX_DELAY);
  if (tft.getTouch(&x, &y)&&TouchRelease==1)
  {
    TouchRelease=0;
    if(bulletTotal>30) {
      if ((x > 50) && (x < (190))) {
        if ((y > 250) && (y <= (320))) {
          reloadCounter=1;
          playWav("/reloadstart.wav");
          bulletTotal=bulletTotal-(30-bullets);
          bullets=30;
          ESP_LOGI(TAG,"Reload");
        }
      }
    }
    if(y>25 && y<=73) { //Menu icons across top of the screen
      playWav("/beep.wav");
      if ((x > 0) && (x < 48)) {
        ScreenMode=1; //info
        xTaskNotify( xUpdateScreen, SU_Main|SU_Taskbar, eSetBits );
      }
      if ((x > 48) && (x < 96)) {
        ScreenMode=2; //Settings
        xTaskNotify( xUpdateScreen, SU_Main|SU_Taskbar, eSetBits );
      }
      if ((x > 96) && (x < 144)) {
        ScreenMode=3; //Map
        xTaskNotify( xUpdateScreen, SU_Main|SU_Taskbar, eSetBits );
      }
      if ((x > 192) && (x < 240)) {
        PowerOff();
      }
    }
    if(ScreenMode==2) { // Settings
      if ((x > 5) && (x < (120))) {
        if ((y > 85) && (y <= (115))) {
          if(FireMode<3) //Burst,Single,Auto
            FireMode++;
          else
            FireMode=0;
          playWav("/beep.wav");
          xTaskNotify( xUpdateScreen, SU_Main, eSetBits );
        }
      }
      if ((x > 5) && (x < (120))) {
        if ((y > 125) && (y <= (155))) {
          if(!Supressor) {
            Supressor=1;
          }
          else {
            Supressor=0;
          }
          playWav("/beep.wav");
          xTaskNotify( xUpdateScreen, SU_Main, eSetBits );
        }
      }
      if ((x > 5) && (x < (120))) {
        if ((y > 165) && (y <= (195))) {
          if(!Laser) {
            Laser=1;
          }
          else {
            Laser=0;
          }
          playWav("/beep.wav");
          xTaskNotify( xUpdateScreen, SU_Main, eSetBits );
        }
      }
    }
  }
  else
    TouchRelease=1;
  xSemaphoreGive(xTFTMutex);
  if(reloadCounter<12)
  {
    if((reloadDelay+500)<millis()) {
      reloadCounter++;
      if(reloadCounter==12) {
        playWav("/reloadend.wav");
        xTaskNotify( xUpdateScreen, SU_Bullets, eSetBits );
      }
      xTaskNotify( xUpdateScreen, SU_Reload, eSetBits );
      reloadDelay=millis();
    }
  }
  /*if(health==0){
    if(deathCounter>0) {
      if((deathDelay+1000)<millis()) {
        deathDelay=millis();
        deathCounter--;
        xTaskNotify( xUpdateScreen, SU_Dead, eSetBits );
      }
    }
    else
    {
      StaticJsonDocument<200> pkt;
      pkt["type"]=PKT_SensorCmd;
      pkt["SensorCmd"]=4;
      pkt["from"]=myMacString;
      pkt["to"]=sensorMac;
      char message[128];
      serializeJson(pkt, message);
      espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
      health=100;
      bullets=30;
      bulletTotal=300;
      playWav("/respawn.wav");
      xTaskNotify( xUpdateScreen, SU_Health|SU_Bullets, eSetBits );
    }
  }*/
  if((gpsDelay+5000)<millis()) {
    gpsDelay=millis();
    //consoleInsert(&head,"Number=%d",gpsDelay);
    xTaskNotify( xUpdateScreen, SU_Main, eSetBits ); //This doesn't belong to GPS, but needs to be called once a second, so convivent to stick it here for now
    xTaskNotify( xUpdateScreen, SU_Wifi, eSetBits );
    while(Serial2.available())
    {
      short gpsloop=0;
      gps.encode(Serial2.read());
      if(gps.location.age() < 10000 && gps.location.isValid()) {
        gps_connected=1;
        //calculate rolling average of gps co-ordinates
        static double lat1,lat2,lat3,lat4,lat5,lon1,lon2,lon3,lon4,lon5;
        lat2=lat1;
        lat3=lat2;
        lat4=lat3;
        lat5=lat4;
        lon2=lon1;
        lon3=lon2;
        lon4=lon3;
        lon5=lon4;
        lat1=gps.location.lat();
        lon1=gps.location.lng();
        lat=(lat1+lat2+lat3+lat4+lat5)/5;
        lon=(lon1+lon2+lon3+lon4+lon5)/5;
        /*Serial.print("LAT=");  Serial.println(gps.location.lat(), 6);
        Serial.print("LONG="); Serial.println(gps.location.lng(), 6);
        Serial.print("ALT=");  Serial.println(gps.altitude.meters());
        Serial.print("AGE=");  Serial.println(gps.location.age());*/
        //break;
      }
      else {
        gps_connected=0;
      }
      gpsloop++;
      if(gpsloop>10)
        break;
    }
    if(MeshState==MESH_CONNECTED) {
      //Send GPS and other player status updates as broadcast if valid 
      StaticJsonDocument<1024> pkt;  
      pkt["type"]=PKT_PlayerUpdate;
      pkt["PlayerID"]=playerID;
      pkt["battery"]=battery_pct;
      pkt["shots"]=shots;
      pkt["deaths"]=PlayerDetails[playerID].deaths;
      JsonArray hitcount = pkt.createNestedArray("hitcount");
      for(int i=1;i<32;i++){
        hitcount.add(PlayerDetails[playerID].hitcount[i]);
      }
      if(gps.location.age()<10000) {
        
        
        pkt["GPSLat"]=lat;
        pkt["GPSLon"]=lon;
        pkt["GPSAge"]=gps.location.age();
        
      }
      pkt["from"]=myMacString;
      pkt["to"]="FF:FF:FF:FF:FF:FF";
      char message[220];
      serializeJson(pkt, message);
      espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 3);
      if(SensorConnected) { //Re-send intended sensor status every 1s to work around issue #42
        if(health) {
          ESP_LOGI(TAG,"Sensor Update");
          StaticJsonDocument<200> pkt;
          pkt["type"]=PKT_SensorCmd;
          pkt["SensorCmd"]=4;
          pkt["from"]=myMacString;
          pkt["to"]=sensorMac;
          char message[128];
          serializeJson(pkt, message);
          Serial.println(message);
          espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
        }
      }
    }
  }
  pinMode(36,INPUT_PULLUP);
  int buttonadc=analogRead(36);
  //Serial.println(buttonadc);
  if(buttonadc<10){
    reloadCounter=1;
    playWav("/reloadstart.wav");
    bulletTotal=bulletTotal-(30-bullets);
    bullets=30;
  }
  if(buttonadc>10&&buttonadc<300){
  
  }
  if(!digitalRead(26)) {//debounce?
    pinMode(26,INPUT_PULLUP);
    if(!digitalRead(26)&&hw_type==1&&health>0) {
      if(((millis()>(bulletdelay+150))&&!Laser)||((millis()>(bulletdelay+400))&&Laser)) {
        LastAction=millis();
        if(SensorConnected||1) {
          if(!bullets) {
            if(!ButtonRelease) {
              playWav("/empty.wav");
              bulletdelay=millis();
              ButtonRelease++;
            }
          }
          else {
            if(FireMode==1) {
              shoot();
            }
            else if((FireMode==0)&&(ButtonRelease<3)) {
              ButtonRelease++;
              shoot();
            }
            else if((FireMode==2)&&(ButtonRelease<1)) {
              ButtonRelease++;
              shoot();
            }
          }
        }
        else {
          //Sensor is not connected, notify player with a beep or something
          playWav("/beep.wav");
        }
      }
    }
  }
  else
    ButtonRelease=0;
  if(barreltemp)
  {
    //barreltemp=barreltemp-((barreltemp-20)/1000);
    if(barreltemp>200)
      barreltemp=barreltemp-(barreltemp/200);
    else
      barreltemp--;
    if(!Laser) {
      if(barreltemp>300)
        FastLED.setBrightness((barreltemp-300)/4);
      else
        FastLED.setBrightness(0);
      for(int i=0;i<8;i++)
        gun_leds[i] = CRGB::Red;
        //delay(10);
      FastLED.show();
      xTaskNotify( xUpdateScreen, SU_Temp, eSetBits );
    }
  }
  if(Laser){
    if((millis()>(bulletdelay+400))){
      FastLED.setBrightness(0);
      FastLED.show();
    }
    else {
      if(((bulletdelay+350)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<1;i++)
          gun_leds[i] = CRGB::Green; 
      }
      if(((bulletdelay+300)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<2;i++)
          gun_leds[i] = CRGB::Green; 
      }
      if(((bulletdelay+250)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<3;i++)
          gun_leds[i] = CRGB::Green; 
      }
      if(((bulletdelay+200)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<4;i++)
          gun_leds[i] = CRGB::Green;
      }
      if(((bulletdelay+150)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<5;i++)
          gun_leds[i] = CRGB::Green;
      }
      if(((bulletdelay+100)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<6;i++)
          gun_leds[i] = CRGB::Green; 
      }
      if(((bulletdelay+50)>millis())) {
        for(int i=0;i<8;i++)
          gun_leds[i] = CRGB::Black;
        for(int i=0;i<7;i++)
          gun_leds[i] = CRGB::Green;
      }
      FastLED.show();   
    }
  }
}