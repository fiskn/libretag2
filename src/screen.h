#ifndef INC_SCREEN
#define INC_SCREEN
#include <TFT_eSPI.h>
#include <TJpg_Decoder.h>
#include "esp_mesh.h"
#include "esp_mesh_internal.h"
#include "screen.h"
#include "structs.h"
#include <TinyGPS++.h>

//enum ScreenUpdate { SU_NULL, SU_Wifi, SU_Bullets, SU_Health, SU_Reload, SU_Taskbar, SU_Battery };
#define SU_Wifi     0b0000000001
#define SU_Bullets  0b0000000010
#define SU_Health   0b0000000100
#define SU_Reload   0b0000001000
#define SU_Taskbar  0b0000010000
#define SU_Battery  0b0000100000
#define SU_Main     0b0001000000
#define SU_Dead     0b0010000000
#define SU_Score    0b0100000000
#define SU_Temp     0b1000000000

extern int playerID,teamID,health,bullets,bulletTotal,battery_pct,SensorConnected,runtime_cal;
extern TFT_eSPI tft;
extern int MeshState,mesh_connected,gps_connected, ScoreTeam1, ScoreTeam2, barreltemp;
extern TaskHandle_t xUpdateScreen;
extern SemaphoreHandle_t xTFTMutex;
extern int voltage,SensorBattery;
extern wifi_ap_record_t wifidata;
extern uint8_t ScreenMode, FireMode, Supressor, Laser, lastrssi;
extern short deathCounter,reloadCounter;
extern TinyGPSPlus gps;
extern NodePointer head;
extern double lat,lon;

void InitScreen();
void updateScreen(void * parameter);
void touch_calibrate();
bool tft_output(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t* bitmap);
#endif /* INC_SCREEN */
