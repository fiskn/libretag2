#include "ota.h"
#include "wifidetails.h"
#include "console.h"
#include <HTTPClient.h>
#include <HTTPUpdate.h>

String host = "gitlab.com"; // Host => bucket-name.s3.region.amazonaws.com
int port = 80; // Non https. For HTTPS 443. As of today, HTTPS doesn't work.
String bin = "/fiskn/libretag2/-/raw/master/data/"; // bin file name with a slash in front.

// Utility to extract header value from headers
String getHeaderValue(String header, String headerName) {
  return header.substring(strlen(headerName.c_str()));
}

void setClock() {
  configTime(0, 0, "pool.ntp.org", "time.nist.gov");  // UTC

  Serial.print(F("Waiting for NTP time sync: "));
  time_t now = time(nullptr);
  while (now < 8 * 3600 * 2) {
    yield();
    delay(500);
    Serial.print(F("."));
    now = time(nullptr);
  }

  Serial.println(F(""));
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print(F("Current time: "));
  Serial.print(asctime(&timeinfo));
}

void displayConsole() {
  int j=0;
  char mystring[40];
  NodePointer current=head;
  tft.fillRect(0,75,240,180,TFT_BLACK);
  tft.setFreeFont(&FreeSans9pt7b);
  tft.setTextSize(1);
  while(NULL != current){
    sprintf(mystring, "%s",current->data);
    tft.drawString(mystring, 0, 75+(j*14), 2);
    current = current->next;
    j++;
  }
}
int ota::connectWifi() {
  tft.drawString("Maint Mode", 0, 0, 2);
  WiFi.begin(OTA_SSID, OTA_PASS);
  while (retries > 0 && WiFi.status() != WL_CONNECTED)
  {
    consoleInsert(&head,"Connecting To Wifi ...");
    displayConsole();
    delay(1000);
    retries--;
  }
  consoleInsert(&head,"Connected To Wifi");
  displayConsole();
  return 0;
}
int ota::flashFirmware() {
  setClock();
  WiFiClientSecure client;
  //client.setCACert(rootCACertificate);

  // Reading data over SSL may be slow, use an adequate timeout
  client.setTimeout(12000 / 1000); // timeout argument is defined in seconds for setTimeout
  t_httpUpdate_return ret = httpUpdate.update(client, "https://server/file.bin");

  switch (ret) {
    case HTTP_UPDATE_FAILED:
      Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
      consoleInsert(&head,"Update Failed!!!");
      displayConsole();
      return -1;
      break;

    case HTTP_UPDATE_NO_UPDATES:
      Serial.println("HTTP_UPDATE_NO_UPDATES");
      break;

    case HTTP_UPDATE_OK:
      Serial.println("HTTP_UPDATE_OK");
      consoleInsert(&head,"Update Success. Rebooting");
      displayConsole();
      delay(1000);
      ESP.restart();
      return 0;
      break;
  }
  return -1;
}

void ota::downloadFile(char* url, char* filename){
  char message[120];
  sprintf(message,"Download %s",filename);
  consoleInsert(&head,"Download %s",filename);
  displayConsole();
  Serial.println(message);
  File UploadFile;
  HTTPClient http;
  http.begin(url);
  int httpCode = http.GET();
  sprintf(message,"HTTP:%d\n",httpCode);
  Serial.println(message);
  //SPIFFS.format();
  if(httpCode > 0) {
    if(httpCode == HTTP_CODE_OK) {
      Serial.println("Start Download\n");
      // get lenght of document (is -1 when Server sends no Content-Length header)
      int len = http.getSize();
      // create buffer for read
      uint8_t buff[1024] = { 0 };
      UploadFile = SPIFFS.open(filename, "w");
      // get tcp stream
      WiFiClient * stream = http.getStreamPtr();
      // read all data from server
      while(http.connected() && (len > 0 || len == -1)) {
        // get available data size
        size_t size = stream->available();
        if(size) {
        // read up to 128 byte
          int c = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
          // write it to Serial
          UploadFile.write(buff, c);
          if(len > 0) {
            len -= c;
          }
        }
          vTaskDelay(1);
      }
      Serial.println("Download Finished\n");
      UploadFile.close();
    }
  }
      http.end();
}
  /*if (!SPIFFS.begin()) {
    Serial.println("SPIFFS.begin() failed\n");
    return;
  }*/
int ota::downloadFiles() {
  char url[100];
  uint8_t MAC_array[6];
  char MAC_char[18];
  WiFi.macAddress(MAC_array);
  /*for (int i = 0; i < sizeof(MAC_array); ++i){
      sprintf(MAC_char,"%s%02x",MAC_char,MAC_array[i]);
    }
  
  
  sprintf(url,"http://192.168.1.207/%s.config",MAC_char);
  sprintf(filename,"/config.txt");
  downloadFile(url,filename);*/
  char filename[20];
  char baseurl[60]="https://gitlab.com/fiskn/libretag2/-/raw/master/data/";
  sprintf(url,"%sshot.wav",baseurl);
  sprintf(filename,"/shot.wav");
  downloadFile(url,filename);
  sprintf(url,"%ssnsrcnt.wav",baseurl);
  sprintf(filename,"/snsrcnt.wav");
  downloadFile(url,filename);
  sprintf(url,"%ssupress.wav",baseurl);
  sprintf(filename,"/supress.wav");
  downloadFile(url,filename);
  sprintf(url,"%sreloadstart.wav",baseurl);
  sprintf(filename,"/reloadstart.wav");
  downloadFile(url,filename);
  sprintf(url,"%sreloadend.wav",baseurl);
  sprintf(filename,"/reloadend.wav");
  downloadFile(url,filename);
  sprintf(url,"%sdamage.wav",baseurl);
  sprintf(filename,"/damage.wav");
  downloadFile(url,filename);
  sprintf(url,"%sdie.wav",baseurl);
  sprintf(filename,"/die.wav");
  downloadFile(url,filename);
  sprintf(url,"%sbeep.wav",baseurl);
  sprintf(filename,"/beep.wav");
  downloadFile(url,filename);
  sprintf(url,"%srespawn.wav",baseurl);
  sprintf(filename,"/respawn.wav");
  downloadFile(url,filename);
  sprintf(url,"%sempty.wav",baseurl);
  sprintf(filename,"/empty.wav");
  downloadFile(url,filename);
  sprintf(url,"%smiss.wav",baseurl);
  sprintf(filename,"/miss.wav");
  downloadFile(url,filename);
  sprintf(url,"%sbullet.jpg",baseurl);
  sprintf(filename,"/bullet.jpg");
  downloadFile(url,filename);
  sprintf(url,"%sheart.jpg",baseurl);
  sprintf(filename,"/heart.jpg");
  downloadFile(url,filename);
  sprintf(url,"%steam.jpg",baseurl);
  sprintf(filename,"/team.jpg");
  downloadFile(url,filename);
  sprintf(url,"%splayer.jpg",baseurl);
  sprintf(filename,"/player.jpg");
  downloadFile(url,filename);
  sprintf(url,"%sinfo.jpg",baseurl);
  sprintf(filename,"/info.jpg");
  downloadFile(url,filename);
  sprintf(url,"%spower.jpg",baseurl);
  sprintf(filename,"/power.jpg");
  downloadFile(url,filename);
  sprintf(url,"%ssettings.jpg",baseurl);
  sprintf(filename,"/settings.jpg");
  downloadFile(url,filename);
  sprintf(url,"%smap.jpg",baseurl);
  sprintf(filename,"/map.jpg");
  downloadFile(url,filename);
  delay(10000);
  return 1;
}
