#include "mesh.h"
#include <EspNowFloodingMesh.h>
#include <FastLED.h>
#include <ArduinoJson.h>

long baseDelay,beaconDelay;
int LEDcounter;

extern CRGB root_leds[33];
extern int teamID;
extern char myMacString[18];
extern char broadcastString[18];

void animateLEDS() {
    CRGB base_colour;
    if(teamID==1) {
        base_colour=CRGB::Red;
    }
    if(teamID==2) {
        base_colour=CRGB::Green;
    }
    if(MeshState!=MESH_CONNECT_NO_ID) {
        base_colour=CRGB::Blue;
    }

    LEDcounter++;
    if(LEDcounter>0&&LEDcounter<4) {
        for(int i=0;i<32;i++)
        {
            root_leds[i] = CRGB::Black;
        }
        for(int i=0;i<8;i++)
        {
            root_leds[i] = base_colour;
        }
        
        FastLED.show();
    }
    if(LEDcounter>3&&LEDcounter<8) {
        for(int i=0;i<32;i++)
        {
            root_leds[i] = CRGB::Black;
        }
        for(int i=8;i<16;i++)
        {
            root_leds[i] = base_colour;
        }
        FastLED.show();
    }
    if(LEDcounter>7&&LEDcounter<11) {
        for(int i=0;i<32;i++)
        {
            root_leds[i] = CRGB::Black;
        }
        for(int i=16;i<24;i++)
        {
            root_leds[i] = base_colour;
        }
        FastLED.show();
    }
    if(LEDcounter>11&&LEDcounter<15) {
        for(int i=0;i<32;i++)
        {
            root_leds[i] = CRGB::Black;
        }
        for(int i=24;i<32;i++)
        {
            root_leds[i] = base_colour;
        }
        FastLED.show();
    }
    if(LEDcounter==15) {
        for(int i=0;i<32;i++)
        {
            root_leds[i] = CRGB::White;
        }
        FastLED.show();
        StaticJsonDocument<200> pkt;
        pkt["type"]=PKT_Beacon;
        pkt["BeaconCmd"]=BC_RESPAWN;
        pkt["TeamID"]=teamID;
        pkt["from"]=myMacString;
        pkt["to"]="FF:FF:FF:FF:FF:FF";
        char message[128];
        serializeJson(pkt, message);
        espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
    }
    if(LEDcounter==16) {
        for(int i=0;i<32;i++)
        {
            root_leds[i] = CRGB::Black;
        }
        FastLED.show();
    }
    if(LEDcounter==17)
        LEDcounter=0;
}

void base_loop() {
    if((baseDelay+100)<millis()) {
        //esp_mesh_set_beacon_interval(100);
        baseDelay=millis();
        animateLEDS();
    }
    if((beaconDelay+2000)<millis()){
        beaconDelay=millis();
        
    }
}