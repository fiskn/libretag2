#include "structs.h"
extern NodePointer head;

void consoleInsert(NodePointer *head, const char* format, ...);
void consoleRemoveHead(NodePointer *head);