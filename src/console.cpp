#include "console.h"

#define TAG "Console"

NodePointer head = NULL;

void consoleRemoveHead(NodePointer *head){
  ESP_LOGE(TAG, "nothere");
  NodePointer temp = NULL;
  NodePointer current = *head;
  //check for empty list
   if(NULL == current){
      ESP_LOGE(TAG, "Cannot delete from empty list!\n");
      return;
   }
   temp = current;
   *head = current->next;
   free(temp);
}

void consoleInsert(NodePointer *head, const char* format, ...) {
  NodePointer newNode = NULL;
  NodePointer previous = NULL;
  NodePointer current = *head;
  static char buffer[40];
  int j=0;
  newNode = malloc(sizeof(Node));
  if(NULL != newNode){
    va_list list;
    va_start(list, format);
    vsprintf(buffer, format, list);
    va_end(list);
    strcpy(newNode->data, buffer);
    while(current!=NULL){
      previous = current;
      current = current->next;
      j++;
      if(j>9)
        consoleRemoveHead(head); 
    }
    if(NULL == previous){
         newNode->next = current;
         //change the address stored in head
         *head = newNode; 
      }//end of if
      else{
      //insert between previous and current
         previous->next = newNode;
         newNode->next = current;
      }//end of else
  }
}