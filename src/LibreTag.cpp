#include "Arduino.h"
#include <ArduinoJson.h>
#include <EspNowFloodingMesh.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"
#include <esp_log.h>
#include <TFT_eSPI.h>
#include <SPI.h>
#include <SPIFFS.h>
#include <SD.h>
#include <TJpg_Decoder.h>
#include "ir.h"
#include "driver/rtc_io.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_pm.h"
#include "AudioFileSourceSPIFFS.h"
#include "AudioGeneratorWAV.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"
#include "AudioOutputMixer.h"
#include "AudioGeneratorTalkie.h"
#include "ota.h"
#include "mesh.h"
#define FASTLED_ESP32_FLASH_LOCK 0
#include <FastLED.h>
#include <TinyGPS++.h>
#include "structs.h"
#include "screen.h"
#include "bases.h"
#include "console.h"
#include "sensors.h"
#include "guns.h"
#include "wire.h"
#include <RTClib.h>
#include "TimeLib.h"
#define USE_RAW_801_11 1
#include <EspNowFloodingMesh.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <DNSServer.h>
#include <ESPui.h>


//#define I2SNODAC
//#define MESH_SET_ROOT

#ifdef MESH_SET_ROOT
const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 4, 1);
DNSServer dnsServer;
#include <WiFi.h>

int millisLabelId;
int UI_GameType, UI_Players;
#endif

//AES 128bit
unsigned char secredKey[] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA,0xBB,0xCC,0xDD,0xEE, 0xFF};
char bsid[6] = {0x0,0x1,0x0,0x1,0x0,0x1};
char broadcastString[18] ="FF:FF:FF:FF:FF:FF";
char myMacString[18];

//include <GUIslice.h>



AudioGeneratorWAV *wav[4];
AudioFileSourceSPIFFS *file[4];
AudioGeneratorTalkie *talkie;
#ifdef I2SNODAC
#include "AudioOutputI2SNoDAC.h"
AudioOutputI2SNoDAC *out;

#else
AudioOutputI2S *out;
#endif
AudioOutputMixer *mixer;
AudioOutputMixerStub *stub[4];
//AudioOutputBuffer *AudioBuffer;

TaskHandle_t xAudio;
SemaphoreHandle_t xSPIFFMutex;
SemaphoreHandle_t xTFTMutex;
TaskHandle_t xUpdateScreen;
TaskHandle_t xMesh;
TaskHandle_t xguns_rx_loop;
TinyGPSPlus gps;
double lat,lon;
wifi_ap_record_t wifidata;
TFT_eSPI tft = TFT_eSPI();

#define NUM_GUN_LEDS 9
#define NUM_ROOT_LEDS 33
#define NUM_HEAD_LEDS 18
#define TAG "LibreTag"
#define MESH_TAG "Mesh"

uint8_t spAFFIRMATIVE[] PROGMEM = {0xA5,0x4F,0x7A,0xD3,0x3C,0x5A,0x8F,0xAE,0xC8,0xA9,0x70,0xED,0xBD,0xBA,0x2A,0x3B,0xC3,0xD9,0x8F,0x00,0x6C,0x4A,0x21,0x40,0xD3,0xCA,0x08,0x18,0xC2,0x04,0x01,0xC3,0x86,0x11,0x60,0xDA,0x4C,0x05,0x54,0x53,0xDA,0x9C,0x58,0x16,0xED,0xC8,0xEB,0x88,0xE2,0x4C,0xEC,0xC1,0x36,0x23,0xC8,0x65,0xD1,0x17,0xBA,0xB4,0x20,0xE5,0xE4,0x6A,0x8A,0x53,0xA2,0xAC,0x0B,0x73,0x38,0xC9,0xC8,0xB2,0x68,0xCE,0x92,0x24,0x33,0x5B,0x45,0xB1,0xA9,0x11,0xB6,0x6A,0x75,0x4D,0x96,0x98,0xC7,0xAA,0xD6,0x37,0x91,0xEC,0x12,0xAF,0xC8,0xD1,0xB1,0x88,0x97,0x25,0x76,0xC0,0x96,0x22,0x01,0xF8,0x2E,0x2C,0x01,0x53,0x99,0xAD,0xA1,0x7A,0x13,0xF5,0x7A,0xBD,0xE6,0xAE,0x43,0xD4,0x7D,0xCF,0xBA,0xBA,0x0E,0x51,0xF7,0xDD,0xED,0x6A,0xB6,0x94,0xDC,0xF7,0xB4,0xB7,0x5A,0x57,0x09,0xDF,0x9D,0xBE,0x62,0xDC,0xD4,0x75,0xB7,0xFB,0xAA,0x55,0x33,0xE7,0x3E,0xE2,0x2B,0xDC,0x5D,0x35,0xFC,0x98,0xAF,0x79,0x0F,0x0F,0x56,0x6D,0xBE,0xE1,0xA6,0xAA,0x42,0xCE,0xFF,0x03};

CRGB gun_leds[NUM_GUN_LEDS];
CRGB root_leds[NUM_ROOT_LEDS];
CRGB head_leds[NUM_HEAD_LEDS];
int leds,bullets=30,bulletTotal=300,health=100,batterycounter=0,runtime_cal=0,ScoreTeam1,ScoreTeam2,barreltemp=0;
int playerID=0,teamID=0,audioOverFlow=0,battery_pct,hw_type=0, mesh_connected=0,SensorConnected=0,MeshState;
short ledFlashCounter,deathCounter,reloadCounter=12;
int gps_connected=0,counter;
long bulletdelay,everySecond,deathDelay,reloadDelay,gpsDelay,gunsDelay,meshDelay,rootDelay,wifiScanDelay;
uint32_t LastAction,lastSensorPKT,lastMeshPKT;
uint8_t ScreenMode, FireMode, ButtonRelease,TouchRelease,Supressor, GameType, beaconCode, Laser, lastrssi;
int voltage,SensorBattery;
class IR IR;
bool wifiRescan,beaconDiscoveryOnly;
int adcval1=0,adcval2=0,adcval3=0,adcval4=0,adcval5=0;

struct PlayerDetail PlayerDetails[32];

extern NodePointer head;

void root_rx(const uint8_t *data, int len, uint32_t replyPrt, uint8_t rssi);

class Config {
private:

public:
  int getScreenType();
};

int Config::getScreenType() {
  return 2;
}

Config config;

RTC_DS3231 rtc;
uint32_t syncProvider()
{
  return rtc.now().unixtime();  //either format works
// DateTime now = RTC.now();
// uint32_t s = now.unixtime();
// return s;
}

void PowerOff() {

  ESP_LOGI(TAG,"Powering Down");
  if(hw_type==1) {
    pinMode(4,OUTPUT);
    digitalWrite(4,1);

    pinMode(13,OUTPUT);
    digitalWrite(13,1);
    pinMode(22,OUTPUT);
    digitalWrite(22,0);
    pinMode(14,OUTPUT);
    digitalWrite(14,0);
    pinMode(12,OUTPUT);
    digitalWrite(12,0);
    pinMode(26,OUTPUT);
    digitalWrite(26,1);
    pinMode(16,OUTPUT);
    digitalWrite(16,0);
    pinMode(17,OUTPUT);
    digitalWrite(17,0);
    pinMode(18,OUTPUT);
    digitalWrite(18,0);
    pinMode(19,OUTPUT);
    digitalWrite(19,0);
    pinMode(23,OUTPUT);
    digitalWrite(23,0);
  }
  if(hw_type==2) {
    pinMode(12,OUTPUT);
    digitalWrite(12,0);
    pinMode(32,OUTPUT);
    digitalWrite(32,0);
  }
  adc_power_off(); // Workaround bug where deep sleep uses 1.5ma instead of 80ua
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_26,0);
  esp_deep_sleep_start();
}

void checkWifiRescan() {
  if(wifiRescan&&((meshDelay+1000)<millis())) {
    wifiRescan=0;
    meshDelay=millis();
    esp_wifi_scan_stop();
    wifi_scan_config_t scan_config = { 0 };
    scan_config.ssid = 0;
    scan_config.bssid = 0;
    scan_config.show_hidden = 1;
    scan_config.scan_type = WIFI_SCAN_TYPE_PASSIVE;
    scan_config.channel=6;
    scan_config.scan_time.passive=240;
    esp_mesh_set_passive_scan_time(240);
    if(hw_type==1)
      esp_mesh_set_beacon_interval(100);
    if(hw_type==2) {
      digitalWrite(22,HIGH);
    }
    esp_wifi_scan_start(&scan_config, 0);
  }
}

void checkPower() {
  float adcVal;
  if(hw_type==1||hw_type==2) {
    if(adcval1==0) {
      adcval1=analogRead(39);
      adcval2=adcval1;
      adcval3=adcval1;
      adcval4=adcval1;
      adcval5=adcval1;
    }
    if(hw_type==1){
      if(!wav[0]->isRunning()&&!wav[1]->isRunning()&&!wav[2]->isRunning()&&!wav[3]->isRunning()) {
        adcval5=adcval4;
        adcval4=adcval3;
        adcval3=adcval2;
        adcval2=adcval1;
        adcval1 = analogRead(39);
      }
    }
    else
    {
      adcval5=adcval4;
      adcval4=adcval3;
      adcval3=adcval2;
      adcval2=adcval1;
      adcval1 = analogRead(39);
    }
    adcVal=(adcval1+adcval2+adcval3+adcval4+adcval5)/5;
    //voltage = (adcVal*(3300/4095)*2)+35;
    voltage = ((adcVal*85*2)/100)+35;
    battery_pct = (1000-(4200-voltage))/10;
    if(battery_pct<0)
      battery_pct=0;
    if(voltage<3300&&hw_type!=3&&hw_type!=4) {
      ESP_LOGI(MESH_TAG, "Low Voltage (%d)- Powering Down",voltage);
      PowerOff();
    }
  }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        ESP_LOGE(TAG, "Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        ESP_LOGE(TAG, "Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
    file.close();
}
//LiquidCrystal lcd(5);



void wavLoopTask(void * parameter)
{
  while(1) {
    if(audioOverFlow) {
      ESP_LOGE(TAG, "Audio Overflow");
      wav[0]->stop();
      stub[0]->stop();
      delete file[0]; file[0]=NULL;
      audioOverFlow=0;

    }
    mixer->loop();
    //taskYIELD();
    if(wav[0]->isRunning())
    {
      if (!wav[0]->loop())
      {
        ESP_LOGE(TAG, "Wav0 Stop");
        wav[0]->stop();
        stub[0]->stop();
        delete file[0]; file[0]=NULL;
      }
    }
    
    
    if(wav[1]->isRunning())
    {
      if (!wav[1]->loop())
      {
        ESP_LOGE(TAG, "Wav1 Stop");
        wav[1]->stop();
        stub[1]->stop();
        delete file[1]; file[1]=NULL;
      }
    }
    if(wav[2]->isRunning())
    {
      if (!wav[2]->loop())
      {
        ESP_LOGE(TAG, "Wav2 Stop");
        wav[2]->stop();
        stub[2]->stop();
        delete file[2]; file[2]=NULL;
      }
    }
    if(wav[3]->isRunning())
    {
      if (!wav[3]->loop())
      {
        ESP_LOGE(TAG, "Wav3 Stop");
        wav[3]->stop();
        stub[3]->stop();
        delete file[3]; file[3]=NULL;
      }
    }
    
    /*vTaskDelay(12);
    if(!wav[0]->isRunning()&&!wav[1]->isRunning()&&!wav[2]->isRunning()&&!wav[3]->isRunning()&&!talkie->isRunning())
    //if(!wav[0]->isRunning()&&!wav[1]->isRunning()&&!wav[2]->isRunning()&&!wav[3]->isRunning())
    {
      vTaskDelay(50);
      //out->stop();
      pinMode(26,INPUT_PULLUP);
      ESP_LOGE(TAG, "Audio Out Stop");
    }
    else
    {
      //mixer->loop();
      pinMode(26,INPUT_PULLUP);
    }*/
    vTaskDelay(12);
  }
  /*else
    vTaskSuspend( xAudio );*/
    //pinMode(26,INPUT_PULLUP);
    
}

/*bool loadConfig() {
  File configFile = SPIFFS.open("/config.txt", "r");
  if (!configFile) {
    Serial.println("Failed to open config file\n");
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    Serial.println("Config file too large\n");
    return false;
  }

  // Allocate a buffer to store contents of the file.
  std::unique_ptr<char[]> buf(new char[size]);

  // We don't use String here because ArduinoJson library requires the input
  // buffer to be mutable. If you don't use ArduinoJson, you may as well
  // use configFile.readString instead.
  configFile.readBytes(buf.get(), size);

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());

  if (!json.success()) {
    Serial.println("Failed parse conf\n");
    return false;
  }
  if(json["player"])
    playerID=json["player"];
  else {
    Serial.println("No PlayerID Defined");
    return false;
  }
  return true;
}*/

void meshTask(void * parameter) {
  InitMesh();
  while(1){
    vTaskDelay(0);
  }
}

void playWav(const char *wavfilename) {
  //while(1) {
    //if(!wav[0]->isRunning()&&!talkie->isRunning())
    if(!wav[0]->isRunning())
    {
      ESP_LOGI(TAG, "Play WAV0");
      file[0] = new AudioFileSourceSPIFFS(wavfilename);
      wav[0]->begin(file[0], stub[0]);
      pinMode(26,INPUT_PULLUP);
      //break;
    }
    else if(!wav[1]->isRunning())
    {
      ESP_LOGI(TAG, "Play WAV1");
      file[1] = new AudioFileSourceSPIFFS(wavfilename);
      wav[1]->begin(file[1], stub[1]);
      pinMode(26,INPUT_PULLUP);
      //break;
    }
    else if(!wav[2]->isRunning())
    {
      ESP_LOGI(TAG, "Play WAV2");
      file[2] = new AudioFileSourceSPIFFS(wavfilename);
      wav[2]->begin(file[2], stub[2]);
      pinMode(26,INPUT_PULLUP);
      //break;
    }/*
    else if(!wav[3]->isRunning())
    {
      file[3] = new AudioFileSourceSPIFFS(wavfilename);
      wav[3]->begin(file[3], stub[3]);
      pinMode(26,INPUT);
      break;
    }*/
    else {
      ESP_LOGI(TAG, "Play WAV Overflow");
      audioOverFlow=1;
      delay(11);
    }
  //}
}

void UI_GameTypeCB(Control* sender, int value){

}

void UI_Reset_Callback(Control* sender, int value){
  StaticJsonDocument<200> pkt;
  pkt["type"]=PKT_GunCMD;
  pkt["cmd"]=1;
  pkt["from"]=myMacString;
  pkt["to"]="FF:FF:FF:FF:FF:FF";
  char message[128];
  serializeJson(pkt, message);
  espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
}

void initFloodingMesh(){
  if(hw_type==1)
      espNowFloodingMesh_RecvCB(guns_net_rx);
    if(hw_type==2)
      espNowFloodingMesh_RecvCB(sensors_rx);
    if(hw_type==3)
      espNowFloodingMesh_RecvCB(root_rx);    
    espNowFloodingMesh_secredkey(secredKey);
    espNowFloodingMesh_begin(1,bsid,false);
    if(hw_type==2)
      espNowFloodingMesh_setToBatteryNode(true);
    if(hw_type==3)
      espNowFloodingMesh_setToMasterRole(true,3); //Set ttl to 3. TIME_SYNC message use this ttl
    espNowFloodingMesh_ErrorDebugCB([](int level, const char *str) {
      Serial.println(str);
    });
    espNowFloodingMesh_syncTimeAndWait(100,3,0);
    uint8_t mac[6];
    esp_wifi_get_mac(WIFI_IF_STA, mac);
    sprintf(myMacString, "%.2X:%.2X:%.2X:%.2X:%.2X:%.2X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    Serial.println(myMacString);
    MeshState=MESH_CONNECT_NO_ID;
}

void setup() {
    esp_log_level_set("*", ESP_LOG_INFO);
    Serial.begin(115200);
    Serial.println();
    everySecond=millis();
    //Try and toggle screen power to make hw detection accurate
    pinMode(4,OUTPUT);
    digitalWrite(4,0);
    digitalWrite(4,1);
    pinMode(23,INPUT_PULLDOWN);
    delay(100);
    if(!digitalRead(23)) {
      hw_type=1;
      ESP_LOGI(TAG, "HW is Tagger\n");
    }
    else {
      hw_type=2;
      ESP_LOGI(TAG, "HW is Sensor\n");
    }
    #ifdef MESH_SET_ROOT
    hw_type=3;
    ESP_LOGI(TAG, "HW is Root Base\n");
    #endif
  //hw_type=4;
  //hw_type=3;
  if (!SPIFFS.begin(true)) {
    ESP_LOGE(TAG, "SPIFFS.begin() failed\n");
    return;
  }
  //if(!SPIFFS.format())
    //ESP_LOGE(TAG, "SPIFFS.format() failed\n");

  Serial.print("Total");
  Serial.println(SPIFFS.totalBytes());
  Serial.print("Used");
  Serial.println(SPIFFS.usedBytes());
  listDir(SPIFFS, "/", 0);


  if(hw_type==1) {
    digitalWrite(33,0);
    digitalWrite(25,1);
    pinMode(33,OUTPUT);
    pinMode(25,OUTPUT);
    pinMode(0,OUTPUT);
    pinMode(2,OUTPUT);
    pinMode(15,OUTPUT);
    digitalWrite(0,0);
    digitalWrite(2,0);
    digitalWrite(15,0);
    xTFTMutex = xSemaphoreCreateMutex();
    Serial2.begin(9600,SERIAL_8N1, 35, 32);
    pinMode(22,OUTPUT);
    pinMode(12,OUTPUT);
    
    pinMode(26,INPUT_PULLUP);
    
    pinMode(14,OUTPUT);
    digitalWrite(14,1);
    digitalWrite(33,0);
    pinMode(33,OUTPUT);
    digitalWrite(12,1);
    pinMode(4,OUTPUT);
    digitalWrite(4,1);
    delay(20);
    digitalWrite(4,0);
    digitalWrite(12,0);
    delay(10);
    digitalWrite(12,1);
    //PowerOff();
    //pinMode(27,OUTPUT);
    //digitalWrite(27,1);
    pinMode(5,OUTPUT);
    //pinMode(0,OUTPUT);
    pinMode(13,OUTPUT);
    InitScreen();
    
    TJpgDec.setCallback(tft_output);
    tft.setSwapBytes(true);
    ledcAttachPin(13, 7);
    ledcSetup(7, 20000, 8);
    ledcWrite(7, 255);

    //PowerOff();
    xTaskCreatePinnedToCore( updateScreen, "updateScreen", 4096, NULL, 2, &xUpdateScreen, 0);
    initFloodingMesh();
    
    #ifdef I2SNODAC
        out = new AudioOutputI2SNoDAC();
        out->SetPinout(-1, -1, 22);
        #else
        audioLogger = &Serial;
        out = new AudioOutputI2S(0,0,8,0);
        out->SetPinout(15, 2, 0);
        ESP_LOGI(TAG, "Startup: Begin Init Audio with DAC");
        #endif
        out->SetGain(2);
        
        out->SetOutputModeMono(true);
        #ifdef I2SNODAC
        out->SetOversampling(32);
        #endif
        ESP_LOGI(TAG, "Startup: Stop Audio");
        //out->stop();
        //AudioBuffer = new AudioOutputBuffer(512, out)
        mixer = new AudioOutputMixer(256, out);
        vTaskDelay(1);
        stub[0] = mixer->NewInput();
        stub[0]->SetGain(1.0);
        stub[0]->SetChannels(1);
        wav[0] = new AudioGeneratorWAV();
        stub[1] = mixer->NewInput();
        stub[1]->SetGain(1.0);
        stub[1]->SetChannels(1);
        wav[1] = new AudioGeneratorWAV();
        stub[2] = mixer->NewInput();
        stub[2]->SetGain(1.0);
        stub[2]->SetChannels(1);
        wav[2] = new AudioGeneratorWAV();
        stub[3] = mixer->NewInput();
        stub[3]->SetGain(1.0);
        stub[3]->SetChannels(1);
        wav[3] = new AudioGeneratorWAV();
        talkie = new AudioGeneratorTalkie();
        talkie->begin(nullptr, stub[0]);
        //const uint8_t spHIGH[]          PROGMEM = {0x04,0xC8,0x7E,0x9C,0x02,0x12,0xD0,0x80,0x06,0x56,0x96,0x7D,0x67,0x4B,0x2C,0xB9,0xC5,0x6D,0x6E,0x7D,0xEB,0xDB,0xDC,0xEE,0x8C,0x4D,0x8F,0x65,0xF1,0xE6,0xBD,0xEE,0x6D,0xEC,0xCD,0x97,0x74,0xE8,0xEA,0x79,0xCE,0xAB,0x5C,0x23,0x06,0x69,0xC4,0xA3,0x7C,0xC7,0xC7,0xBF,0xFF,0x0F};
 
        uint8_t spFOURTEEN[]  PROGMEM = {0x0C,0x58,0xAE,0x5C,0x01,0xD9,0x87,0x07,0x51,0xB7,0x25,0xB3,0x8A,0x15,0x2C,0xF7,0x1C,0x35,0x87,0x4D,0xB2,0xDD,0x53,0xCE,0x28,0x2B,0xC9,0x0E,0x97,0x2D,0xBD,0x2A,0x17,0x27,0x76,0x8E,0xD2,0x9A,0x6C,0x80,0x94,0x71,0x00,0x00,0x02,0xB0,0x58,0x58,0x00,0x9E,0x0B,0x0A,0xC0,0xB2,0xCE,0xC1,0xC8,0x98,0x7A,0x52,0x95,0x24,0x2B,0x11,0xED,0x36,0xD4,0x92,0xDC,0x4C,0xB5,0xC7,0xC8,0x53,0xF1,0x2A,0xE5,0x1A,0x17,0x55,0xC5,0xAF,0x94,0xBB,0xCD,0x1C,0x26,0xBF,0x52,0x9A,0x72,0x53,0x98,0xFC,0xC2,0x68,0xD2,0x4D,0x61,0xF0,0xA3,0x90,0xB6,0xD6,0x50,0xC1,0x8F,0x42,0xDA,0x4A,0x43,0x39,0x3F,0x48,0x2D,0x6B,0x33,0xF9,0xFF};
        
        ESP_LOGI(TAG, "Startup: Mixer Created");
        //talkie = new AudioGeneratorTalkie();
        //talkie->begin(nullptr, stub[0]);
        xTaskCreatePinnedToCore( wavLoopTask, "WavLoop", 8192, NULL, 30, &xAudio, 1);
        
        ESP_LOGI(TAG, "Startup: Audio loop created");
        vTaskDelay(1);
        ESP_LOGI(TAG, "Startup: Init IR");
        IR.tx_init();
        ScreenMode=1;
        ESP_LOGI(TAG, "Startup: Init console");
        consoleInsert(&head,"Startup Successful");
        //const uint8_t spHIGH[]          PROGMEM = {0x04,0xC8,0x7E,0x9C,0x02,0x12,0xD0,0x80,0x06,0x56,0x96,0x7D,0x67,0x4B,0x2C,0xB9,0xC5,0x6D,0x6E,0x7D,0xEB,0xDB,0xDC,0xEE,0x8C,0x4D,0x8F,0x65,0xF1,0xE6,0xBD,0xEE,0x6D,0xEC,0xCD,0x97,0x74,0xE8,0xEA,0x79,0xCE,0xAB,0x5C,0x23,0x06,0x69,0xC4,0xA3,0x7C,0xC7,0xC7,0xBF,0xFF,0x0F};
        ESP_LOGI(TAG, "Startup: Saying a word");
        talkie->say(spAFFIRMATIVE, sizeof(spAFFIRMATIVE),true);
        //talkie->say(spAFFIRMATIVE, sizeof(spAFFIRMATIVE));

    FastLED.addLeds<WS2812B, 27, GRB>(gun_leds, NUM_GUN_LEDS);
    for(int i=0;i<8;i++)
    {
        gun_leds[i] = CRGB::Red;
    }
    FastLED.show();
    delay(200);
    for(int i=0;i<8;i++)
    {
        gun_leds[i] = CRGB::Green;
    }
    FastLED.show();
    delay(200);
    for(int i=0;i<8;i++)
    {
        gun_leds[i] = CRGB::Blue;
    }
    FastLED.show();
    delay(200);
    for(int i=0;i<8;i++)
    {
        gun_leds[i] = CRGB::Black;
        //FastLED.show();
        //delay(10);
    }
    FastLED.show();
    
    

        //WiFi.mode(WIFI_STA);
        //digitalWrite(22,1);
        xSPIFFMutex = xSemaphoreCreateMutex();

        if(tft.getTouchRawZ())
        {

        }
        delay(500);
        if(!digitalRead(26)) {
          delay(5000);
          if(!digitalRead(26)) {
            uint16_t x,y;
            runtime_cal=1;
            touch_calibrate();
            runtime_cal=0;
            TJpgDec.drawFsJpg(0,25,"/info.jpg");
            TJpgDec.drawFsJpg(48,25,"/settings.jpg");
            TJpgDec.drawFsJpg(95,25,"/map.jpg");
            TJpgDec.drawFsJpg(192,25,"/power.jpg");
            while(1) {
              checkPower();
              if(millis()-LastAction>10000) //Power off after 10s of no screen presses
                PowerOff(); 
              if (tft.getTouch(&x, &y)&&TouchRelease==1)
              {
                LastAction=millis();
                TouchRelease=0;
                if(y>25 && y<=73) { //Menu icons across top of the screen
                  //playWav("/beep.wav");
                  if ((x > 0) && (x < 48)) {
                    break;
                  }
                  if ((x > 48) && (x < 96)) {
                    ota ota;
                    ota.connectWifi();
                    ota.downloadFiles();
                  }
                  if ((x > 96) && (x < 144)) {
                    ota ota;
                    ota.connectWifi();
                    ota.flashFirmware();
                  }
                  if ((x > 192) && (x < 240)) {
                    PowerOff();
                  }
                }
              }
              delay(500);
              TouchRelease=1;
              //FastLED.setBrightness(200);
              //head_leds[0] = CRGB::Blue;
              //FastLED.show();

            }
          }
        }
        vTaskDelay(1);
        ESP_LOGI(TAG, "Startup: Pre touch calibration");
        touch_calibrate();
        ESP_LOGI(TAG, "Startup: Post touch calibration");
        xTaskNotify( xUpdateScreen, SU_Taskbar|SU_Health|SU_Bullets|SU_Reload|SU_Score, eSetBits );
        ESP_LOGI(TAG, "Startup: Updated Screen");
        
        ESP_LOGI(TAG, "Startup: Starting NET");
        vTaskDelay(1);
        //xTaskCreatePinnedToCore( meshTask, "MeshLoop", 4096, NULL, 2, &xMesh, 1);
        //xTaskCreatePinnedToCore( guns_net_rx, "guns_net_rx", 4096, NULL, 2, &xguns_rx_loop, 0);
        ESP_LOGI(TAG, "Startup: Startup Complete");
  }
  if(hw_type==2) {
    pinMode(27,INPUT);
    pinMode(22,OUTPUT);
    pinMode(12,OUTPUT);
    digitalWrite(12,1);
    pinMode(32,OUTPUT);
    pinMode(26,INPUT_PULLUP);
    digitalWrite(32,HIGH);
    initFloodingMesh();
    FastLED.addLeds<WS2812B, 33, GRB>(head_leds, NUM_HEAD_LEDS);
    FastLED.setBrightness(255);
    for(int i=0;i<18;i++)
    {
      head_leds[i] = CRGB::Red;
      FastLED.show();
      delay(10);
    }
    digitalWrite(32,LOW);
    for(int i=0;i<18;i++)
    {
      head_leds[i] = CRGB::Green;
      FastLED.show();
      delay(10);
    }
    digitalWrite(32,HIGH);
    for(int i=0;i<18;i++)
    {
      head_leds[i] = CRGB::Blue;
      FastLED.show();
      delay(10);
    }
    digitalWrite(32,LOW);
    for(int i=0;i<18;i++)
    {
      head_leds[i] = CRGB::Black;
      FastLED.show();
      delay(10);
    }
    IR.rx_init();

  }
  
  if(hw_type==3) {
    pinMode(27,INPUT);
    pinMode(22,OUTPUT);
    Wire.begin(26,25);
    if (! rtc.begin()) {
      Serial.println("Couldn't find RTC");
      Serial.flush();
      while (1) delay(10);
    }
    timeval epoch = {rtc.now().unixtime(), 0};
    settimeofday((const timeval*)&epoch, 0); 
    initFloodingMesh();
        #ifdef I2SNODAC
        out = new AudioOutputI2SNoDAC();
        out->SetPinout(-1, -1, 22);
        #else
        out = new AudioOutputI2S();
        out->SetPinout(15, 2, 0);
        #endif
        out->SetGain(3);
        ESP_LOGI(TAG, "Startup: Begin Init Audio");
        out->SetOutputModeMono(true);
        #ifdef I2SNODAC
        out->SetOversampling(32);
        #endif
        ESP_LOGI(TAG, "Startup: Stopping Audio Output");
        out->stop();
        //AudioBuffer = new AudioOutputBuffer(512, out)
        ESP_LOGI(TAG, "Startup: Create Audio Mixer");
        mixer = new AudioOutputMixer(32, out);

        stub[0] = mixer->NewInput();
        stub[0]->SetGain(1.0);
        stub[0]->SetChannels(1);
        wav[0] = new AudioGeneratorWAV();
        stub[1] = mixer->NewInput();
        stub[1]->SetGain(1.0);
        stub[1]->SetChannels(1);
        wav[1] = new AudioGeneratorWAV();
        stub[2] = mixer->NewInput();
        stub[2]->SetGain(1.0);
        stub[2]->SetChannels(1);
        wav[2] = new AudioGeneratorWAV();
        stub[3] = mixer->NewInput();
        stub[3]->SetGain(1.0);
        stub[3]->SetChannels(1);
        wav[3] = new AudioGeneratorWAV();
        ESP_LOGI(TAG, "Startup: Creating WAV loop task");
        xTaskCreatePinnedToCore( wavLoopTask, "WavLoop", 4096, NULL, 30, &xAudio, 1);
        playWav("/powerup.mp3");
        ESP_LOGI(TAG, "Startup: Configuring LEDs");
    FastLED.addLeds<WS2812B, 33, GRB>(root_leds, NUM_ROOT_LEDS);
    FastLED.setBrightness(255);
    ESP_LOGI(TAG, "Startup: LED1");
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Red;
      FastLED.show();
      delay(10);
    }
    digitalWrite(32,LOW);
    ESP_LOGI(TAG, "Startup: LED2");
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Green;
      FastLED.show();
      delay(10);
    }
    digitalWrite(32,HIGH);
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Blue;
      FastLED.show();
      delay(10);
    }
    digitalWrite(32,LOW);
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Black;
      FastLED.show();
      delay(10);
    }
    FastLED.show();
    /*ledcSetup(1, 200, 8);
    ledcAttachPin(22, 1);
    ledcWriteTone(1, 500);
    ledcWrite(1, 5);
    delay(250);
    ledcWrite(1, 0);
    ledcWriteTone(1, 1000);
    ledcWrite(1, 5);
    delay(250);
    ledcWrite(1, 0);
    ledcWriteTone(1, 2000);
    ledcWrite(1, 5);
    delay(500);
    ledcWrite(1, 0);*/
    //playWav("/powerup.mp3"); 
    IR.rx_init();
  }

  if(hw_type==4) {
    if(!digitalRead(23)) {
      teamID=1;
    }
    else {
      teamID=2;
    }
    initFloodingMesh();
    FastLED.addLeds<WS2812B, 4, GRB>(root_leds, NUM_ROOT_LEDS);
    FastLED.setBrightness(255);
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Red;
      FastLED.show();
      delay(10);
    }
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Green;
      FastLED.show();
      delay(10);
    }
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Blue;
      FastLED.show();
      delay(10);
    }
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Black;
      FastLED.show();
      delay(10);
    }
  }
  //xTaskCreatePinnedToCore( meshTask, "MeshLoop", 4096, NULL, 1, &xMesh, 0);
  //InitMesh();
  #ifdef MESH_SET_ROOT
  //ESPUI.setVerbosity(Verbosity::VerboseJSON);
  WiFi.mode(WIFI_MODE_APSTA);
  delay(100);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  #if defined(ESP32)
  uint32_t chipid = 0;
  for (int i = 0; i < 17; i = i + 8)
  {
      chipid |= ((ESP.getEfuseMac() >> (40 - i)) & 0xff) << i;
  }
  #else
  uint32_t chipid = ESP.getChipId();
  #endif
  char ap_ssid[25];
  snprintf(ap_ssid, 26, "ESPUI-%08X", chipid);
  WiFi.softAP(ap_ssid);

  uint8_t timeout = 5;

  do
  {
      delay(500);
      Serial.print(".");
      timeout--;
  } while (timeout);
  dnsServer.start(DNS_PORT, "*", apIP);

  Serial.println("\n\nWiFi parameters:");
  Serial.print("Mode: ");
  Serial.println(WiFi.getMode() == WIFI_AP ? "Station" : "Client");
  Serial.print("IP address: ");
  Serial.println(WiFi.getMode() == WIFI_AP ? WiFi.softAPIP() : WiFi.localIP());
  uint16_t tab1 = ESPUI.addControl(ControlType::Tab, "Main", "Main");
  millisLabelId = ESPUI.label("Millis:", ControlColor::Emerald, "0");
  UI_GameType = ESPUI.addControl( ControlType::Select, "Game Type", "Normal", ControlColor::Alizarin, tab1, UI_GameTypeCB );
  ESPUI.addControl( ControlType::Option, "Normal", "1", ControlColor::Alizarin, UI_GameType);
  ESPUI.addControl( ControlType::Option, "Capture The Flag", "2", ControlColor::Alizarin, UI_GameType);
  ESPUI.addControl( ControlType::Option, "King Of The Hill", "3", ControlColor::Alizarin, UI_GameType);
  UI_Players=ESPUI.addControl( ControlType::Label, "Scores", "<table></table>", ControlColor::Alizarin,tab1);
  auto colourtab = ESPUI.addControl(Tab, "", "Colours");
	ESPUI.addControl(Button, "Reset All", "Reset All", Alizarin, colourtab, UI_Reset_Callback);
  ESPUI.begin("ESPUI Control");
  #endif
}

  

void generic_loop() {

}

void root_rx(const uint8_t *data, int len, uint32_t replyPrt, uint8_t rssi) {
  uint8_t mac[6];
  esp_wifi_get_mac(WIFI_IF_STA, mac);
  //ESP_LOGI(MESH_TAG, "My MAC: " MACSTR ,MAC2STR(mac));
  //if(meshdata_rx.size&&memcmp(&mac,&from.addr,sizeof(mac))!=0) {
  //if(1) {
    //ESP_LOGI(MESH_TAG, "Mesh data recieved:" MACSTR " :%s", MAC2STR(from.addr), meshdata_rx.data);
  StaticJsonDocument<1024> pkt;
  StaticJsonDocument<200> pkt2;
  deserializeJson(pkt, data);
  ESP_LOGI(MESH_TAG, "Mesh data recieved:%s", data);
  char* from=pkt["from"];
  switch(pkt["type"].as<int>()){
    case PKT_IDReq:
      //Find existing or free player ID
      pkt2["type"]=PKT_IDOffer;
      for(int i=1;i<32;i++) {
        if(memcmp(&PlayerDetails[i].macAddr,from,sizeof(PlayerDetails[i].macAddr))==0){
          pkt2["PlayerID"] = i;
          if(i % 2 == 0) {
            pkt2["TeamID"] = 1;
            PlayerDetails[i].team=1;
          }  
          else {
            pkt2["TeamID"] = 2;
            PlayerDetails[i].team=2;
          }
          ESP_LOGI(MESH_TAG, "Sending Existing PlayerID:%d to %s",i,from);
          break;
        }
        if(PlayerDetails[i].macassigned==0) {
          pkt2["PlayerID"] = i;
          if(i % 2 == 0) {
            pkt2["TeamID"] = 1;
            PlayerDetails[i].team=1;
          }  
          else {
            pkt2["TeamID"] = 2;
            PlayerDetails[i].team=2;
          }
          ESP_LOGI(MESH_TAG, "Sending PlayerID:%d to %s",i,from);
          PlayerDetails[i].macassigned=1;
          memcpy(&PlayerDetails[i].macAddr,from,sizeof(PlayerDetails[i].macAddr));
          break;
        }
      }
      pkt2["from"]=myMacString;
      pkt2["to"]=from;
      char message[128];
      serializeJson(pkt2, message);
      espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 3);
      break;
    case PKT_PlayerUpdate:
      for(int i=1;i<32;i++) {
        if(memcmp(&PlayerDetails[i].macAddr,from,sizeof(PlayerDetails[i].macAddr))==0){
          PlayerDetails[i].lastseen=millis();
          PlayerDetails[i].shots=pkt["shots"].as<int>();
          PlayerDetails[i].deaths=pkt["deaths"].as<int>();
          PlayerDetails[i].battery=pkt["battery"].as<int>();
          for(int j=1;j<32;j++){
            PlayerDetails[i].hitcount[j]=pkt["hitcount"][j];
          }
        }
        
      }
      break;
    default:
      ESP_LOGE(MESH_TAG, "Unhandled Packet");
      //ESP_LOGI(MESH_TAG, "Mesh data recieved:" MACSTR " :%s", MAC2STR(from.addr), meshdata_rx.data);
      break;
  }
}

void root_loop() {
  delay(100);
  #ifdef MESH_SET_ROOT
  dnsServer.processNextRequest();
  char playerInfoString[1024];
  snprintf(playerInfoString,256,"<table border=1><tr><td>ID</td><td>Team</td><td>MAC</td><td>Shots</td><td>Hits</td><td>Deaths</td><td>Last Seen</td><td>Battery</td></tr>");
  for(int i=1;i<32;i++) {
    if(PlayerDetails[i].lastseen>0){
      uint8_t hits;
      for(int j=1;j<32;j++){
        hits=hits+PlayerDetails[j].hitcount[i];
      }
      snprintf(playerInfoString,1024,"%s<tr><td>%d</td><td>%d</td><td>%s</td><td>%d</td><td>%d</td><td></td><td>%d</td><td>%d</td></tr>",playerInfoString,i,PlayerDetails[i].team,PlayerDetails[i].macAddr,PlayerDetails[i].shots,hits,PlayerDetails[i].deaths,(millis()-PlayerDetails[i].lastseen)/1000,PlayerDetails[i].battery);
    }
  }
  snprintf(playerInfoString,1024,"%s</table>",playerInfoString);
  ESPUI.print(millisLabelId, String(millis()));
  ESPUI.print(UI_Players, playerInfoString);
  #endif
  int data=0, flag = 0;
  data=IR.rx_check(); //Call function to check ringbuffer for new IR data
  if (data!=0) {
    /*for(int i=0;i<33;i++)
        {
          root_leds[i] = CRGB::Blue;
        }
        FastLED.show();*/
    if(data==-1) { //Checksum or data corruption, missed
      //play missed sound
      ESP_LOGD(TAG,"Near miss");
      playWav("/miss.mp3");
    }
    if(data>0) { //Its a hit, lets check some more details
      //IR.getPlayerFromBullet(data) //Used for player stats?
      if(IR.getTeamFromBullet(data)==1) {
        for(int i=0;i<33;i++)
        {
          root_leds[i] = CRGB::Red;
        }
        FastLED.show();
        ScoreTeam1++;
        playWav("/powerup.mp3");  
      }
      if(IR.getTeamFromBullet(data)==2) {
        for(int i=0;i<33;i++)
        {
          root_leds[i] = CRGB::Green;
        }
        FastLED.show();
        ScoreTeam2++;
        playWav("/powerup.mp3");         
      }
    }
  }
  if((rootDelay+1000)<millis()) {
    timeval epoch = {rtc.now().unixtime(), 0};
    settimeofday((const timeval*)&epoch, 0);
    /*Serial.print("System Time:");
    Serial.println(time(NULL));
    Serial.print("RTC Time:");
    Serial.println(rtc.now().unixtime());*/
    rootDelay=millis();
    StaticJsonDocument<200> pkt;
    pkt["type"]=PKT_GameType;
    pkt["GameType"]=GameType;
    pkt["to"]="FF:FF:FF:FF:FF:FF";
    char message[64];
    serializeJson(pkt, message);
    espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 3);
    pkt.clear();
    pkt["type"]=PKT_ScoreUpdate;
    if(GameType==3){
      pkt["Team1"]=ScoreTeam1;
      pkt["Team2"]=ScoreTeam2;
    }
    if(GameType==1){
      uint8_t Team1TotalKills,Team2TotalKills;
      for(int i=0;i<32;i++){
        if(PlayerDetails[i].team==1){
          Team2TotalKills+=PlayerDetails[i].deaths;
        }
        else{
          Team1TotalKills+=PlayerDetails[i].deaths;
        }        
      }
      pkt["Team1"]=Team1TotalKills;
      pkt["Team2"]=Team2TotalKills;
    }
    pkt["to"]="FF:FF:FF:FF:FF:FF";
    serializeJson(pkt, message);
    espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 3);
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Blue;
    }
    FastLED.show();
    delay(50);
    for(int i=0;i<33;i++)
    {
      root_leds[i] = CRGB::Black;
    }
    FastLED.show();

  }
}

/*void checkBeaconCode() {
  if(((beaconCode & BC_RESPAWN1)==BC_RESPAWN1 && teamID==1) || ((beaconCode & BC_RESPAWN2)==BC_RESPAWN2 && teamID==2)) {
    //Own Base
    playWav("/beep.mp3");
    xTaskNotify( xUpdateScreen, SU_Bullets, eSetBits );
    if(health==0) {
      health=100;
      bullets=30;
      bulletTotal=300;
    }
    else {
      if(bulletTotal<290)
        bulletTotal=+10;
      else
        bulletTotal=300;
    }
  }
  if(((beaconCode & BC_RESPAWN1)==BC_RESPAWN1 && teamID==2) || ((beaconCode & BC_RESPAWN2)==BC_RESPAWN2 && teamID==1)) {
    //Enemy base, capture flag or something to be implemented here
  }
  beaconCode=0;
}*/

void loop() {
    //yield();
    
    espNowFloodingMesh_loop();
    delay(10);
    batterycounter++;
    counter++;
    if((everySecond+1000)<millis()) {
      everySecond=millis();
      //ESP_LOGI(TAG, "Mem: %d",esp_get_free_heap_size());
      checkPower();
    }
    if(hw_type==2) {
      sensors_loop();
      //checkWifiRescan();
      if(millis()-LastAction>120000) //Power off after 120s for now
        PowerOff();
    }
    if(hw_type==3) {
      root_loop();

    }
    if(hw_type==4) {
      //checkWifiRescan();
      base_loop();

    }
    if(counter>100)
    {

    }
    /*if(hw_type==1) {
      guns_net_rx();
    }*/
    if(hw_type==1&&(millis()>(gunsDelay+100))) {
      gunsDelay=millis();
      if(millis()>(wifiScanDelay+1000)){
        //wifiRescan=1;
        //beaconDiscoveryOnly=1;
        //wifiScanDelay=millis();
      }
      //checkWifiRescan();
      //checkBeaconCode();
      guns_loop();
      //esp_wifi_sta_get_ap_info(&wifidata);
      xTaskNotify( xUpdateScreen, SU_Wifi|SU_Battery, eSetBits );
      if(millis()-LastAction>240000) //Power off after 120s for now
        PowerOff(); 
    }
  }

bool loopTaskWDTEnabled;
TaskHandle_t loopTaskHandle = NULL;

void loopTask(void *pvParameters)
{
    setup();
    for(;;) {
      vTaskDelay(1);
        if(loopTaskWDTEnabled){
            esp_task_wdt_reset();
        }
        loop();
        if (serialEventRun) serialEventRun();
    }
}

extern "C" void app_main()
{
    loopTaskWDTEnabled = true;
    initArduino();
    xTaskCreateUniversal(loopTask, "loopTask", 32768, NULL, 1, &loopTaskHandle, 0);
}

