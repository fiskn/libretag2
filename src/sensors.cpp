#include "sensors.h"
#include <Arduino.h>
#include <ArduinoJson.h>
#include <EspNowFloodingMesh.h>
#include "mesh.h"
#include <FastLED.h>
#include "ir.h"

#define TAG "Sensors"
#define MESH_TAG "Mesh"
#define NUM_HEAD_LEDS 18

extern int playerID,teamID,health,bullets,bulletTotal,battery_pct,SensorConnected,runtime_cal,voltage;
extern uint32_t LastAction;
extern CRGB head_leds[NUM_HEAD_LEDS];
extern class IR IR;
long sensorsDelay;
extern short ledFlashCounter;
char gunMac[18],target_practice=1;
extern char myMacString[18];
extern char broadcastString[18];

void sensors_rx(const uint8_t *data, int len, uint32_t replyPrt, uint8_t rssi) {
//ESP_LOGI(MESH_TAG, "Mesh data recieved:" MACSTR " :%s", MAC2STR(from.addr), meshdata_rx.data);
  ESP_LOGI(MESH_TAG, "Mesh data recieved:%s - RSSI:%d", data,rssi);
  StaticJsonDocument<200> pkt;
  deserializeJson(pkt, data);
  char* to;
  if(pkt.containsKey("to"))
    to=pkt["to"];
  else
    to = "00:00:00:00:00:00";
  //ESP_LOGI(MESH_TAG, "me:%c to:%c bc:%c",myMacString[0],to[0],broadcastString[0]);
  //ESP_LOGI(MESH_TAG, "Compare1:%d Compare2:%d",memcmp(&myMacString,to,1),memcmp(&broadcastString,to,1));
  if(memcmp(&myMacString,to,sizeof(myMacString))!=0&&memcmp(&broadcastString,to,sizeof(broadcastString))!=0){
    //ESP_LOGI(MESH_TAG, "Pkt not for me!!");
    return;
  }
  char* from=pkt["from"];
  switch(pkt["type"].as<int>()){
    case PKT_SensorCmd: {
      if(pkt["SensorCmd"].as<int>()==1) {
        playerID=pkt["PlayerID"];
        teamID=pkt["TeamID"];
        //strcpy(gunMac,pkt["from"]);
        memcpy(&gunMac,from,sizeof(gunMac));
        ESP_LOGI(MESH_TAG, "PlayerID set to: %d", playerID);
        MeshState=MESH_CONNECTED;
        health=1;
        FastLED.setBrightness(0);
        for(int i=0;i<18;i++)
        {
          head_leds[i] = CRGB::Black;
        }
        FastLED.show();
        digitalWrite(32,HIGH);
        delay(100);
        digitalWrite(32,LOW);
      }
      if(pkt["SensorCmd"].as<int>()==2) {
        FastLED.setBrightness(255);
        for(int i=0;i<18;i++)
        {
          head_leds[i].setRGB(pkt["Red"].as<int>(), pkt["Green"].as<int>(), pkt["Blue"].as<int>());
        }
        FastLED.show();
        digitalWrite(32,HIGH);
        delay(100);
        digitalWrite(32,LOW);
        FastLED.setBrightness(0);
        for(int i=0;i<18;i++)
        {
            head_leds[i] = CRGB::Black;
        }
        FastLED.show();
      }
      if(pkt["SensorCmd"].as<int>()==3) {
        health=0;
      }
      if(pkt["SensorCmd"].as<int>()==4) {
        LastAction=millis();
        if(health==0) {
          health=1;
          FastLED.setBrightness(0);
          for(int i=0;i<18;i++)
          {
            head_leds[i] = CRGB::Black;
          }
          FastLED.show();
          digitalWrite(32,HIGH);
          delay(100);
          digitalWrite(32,LOW);
          delay(100);
          digitalWrite(32,HIGH);
          delay(100);
          digitalWrite(32,LOW);
        }
      }
    }
    break;
  default:
    ESP_LOGE(MESH_TAG, "Unhandled Packet");
  break;
  }
}

void sensors_loop() {
  int data = 0;
  int flag = 0;
  delay(1);
  data=IR.rx_check(); //Call function to check ringbuffer for new IR data
  if (data!=0) {
    if(data==-1) { //Checksum or data corruption, missed
      ESP_LOGI(TAG,"Near miss");
      //Send Near Miss
      StaticJsonDocument<200> pkt;
      pkt["type"]=PKT_SensorRX;
      pkt["hit"]=0;
      pkt["from"]=myMacString;
      pkt["to"]=gunMac;
      char message[128];
      serializeJson(pkt, message);
      espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
    }
    if(data>0) { //Its a hit, lets check some more details
      
      if(target_practice) {
        ESP_LOGI(TAG,"Hit");
        for(int i=0;i<18;i++)
          head_leds[i] = CRGB::Red;
          FastLED.setBrightness(255);
        FastLED.show();
        delay(100);
        for(int i=0;i<18;i++)
          head_leds[i] = CRGB::Green;
          FastLED.setBrightness(50);
        FastLED.show();
        LastAction=millis();
      }
      if(IR.getPlayerFromBullet(data)!=playerID) { //Check for reflections from self
        if(IR.getTeamFromBullet(data)!=teamID) { //Friendly fire
          ESP_LOGI(TAG,"Hit");
          StaticJsonDocument<200> pkt;
          pkt["type"]=PKT_SensorRX;
          pkt["hit"]=1;
          pkt["player"]=IR.getPlayerFromBullet(data);
          pkt["team"]=IR.getTeamFromBullet(data);
          pkt["from"]=myMacString;
          pkt["to"]=gunMac;
          char message[128];
          serializeJson(pkt, message);
          espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
        }
        else
        {
          //friendly fire not implemented
        }
      }
    }

    Serial.println(data);
  }
  if(((sensorsDelay+6000)<millis())&&MeshState==MESH_CONNECTED) {
    sensorsDelay=millis();
    StaticJsonDocument<200> pkt;
    pkt["type"]=PKT_SensorUpd;
    pkt["Battery"]=voltage;
    pkt["from"]=myMacString;
    pkt["to"]=gunMac;
    char message[128];
    serializeJson(pkt, message);
    espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
  }
  if(((sensorsDelay+500)<millis())&&MeshState==MESH_CONNECT_NO_ID) {
    sensorsDelay=millis();
    StaticJsonDocument<200> pkt;
    pkt["type"]=PKT_SensorReg;
    pkt["from"]=myMacString;
    pkt["to"]="FF:FF:FF:FF:FF:FF";
    char message[128];
    serializeJson(pkt, message);
    espNowFloodingMesh_send((uint8_t*)message, sizeof(message), 0);
    //meshdata_tx.tos = MESH_TOS_DEF;
    ESP_LOGI(MESH_TAG,"Sending Sensor Registration");
  }
      
  //Flash colours when dead
  if(!health){
    ledFlashCounter++;
    if(ledFlashCounter==10) {
      FastLED.setBrightness(255);
      for(int i=0;i<18;i++)
      {
        head_leds[i] = CRGB::Red;
      }
      FastLED.show();
    }
    if(ledFlashCounter==20) {
      FastLED.setBrightness(255);
      for(int i=0;i<18;i++)
      {
        head_leds[i] = CRGB::Blue;
      }
      FastLED.show();
    }
    if(ledFlashCounter==30) {
      FastLED.setBrightness(255);
      for(int i=0;i<18;i++)
      {
        head_leds[i] = CRGB::Green;
      }
      FastLED.show();
    }
    if(ledFlashCounter==40) {
      FastLED.setBrightness(0);
      for(int i=0;i<18;i++)
      {
        head_leds[i] = CRGB::Black;
      }
      FastLED.show();
    }
    if(ledFlashCounter==50)
      ledFlashCounter=0;
  }

}